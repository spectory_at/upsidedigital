const pathPrefix = require("./package.json");
const baseConfig = require("./base-config.json");

let baseUrl = "";

if (pathPrefix.baseUrl !== undefined) {
    baseUrl = pathPrefix.baseUrl;
}

module.exports = {
    pathPrefix: baseUrl,
    siteMetadata: {
        title: `Upside Digital - Digitize your supply chain!`,
        description: ``,
        keywords: '',
        author: `spectory`,
        siteUrl: `https://upsidedigital.com`,
        defaultImage: `https://wp.upsidedigital.com/wp-content/uploads/2021/08/shutterstock_1016500210.jpg`
    },
    plugins: [
        {
            resolve: "gatsby-plugin-google-tagmanager",
            options: {
              id: "GTM-K87G7JG",
            },
          },
        {
            resolve: `gatsby-source-wordpress`,
            options: {
                url: baseConfig.graphQLUrl,
                includedRoutes: [
                    "**/pages",
                    "**/media",
                    "**/tags",
                    "**/users",
                    "**/jobs",
                ],
            },
        },
        `gatsby-plugin-sass`,
        `gatsby-plugin-image`,
        {
            resolve: `gatsby-plugin-sitemap`,
            options: {
                excludes: [
                    '/404-2',
                    '/en/404-2',
                    '/404-page-not-found',
                    '/en/404-page-not-found',
                    '/case-study-list-page-en',
                    '/en/case-study-list-page-en',
                    '/home',
                    '/en/home',
                    '/test',
                    '/en/test',
                    '/references/senna',
                    '/en/references/senna'
                ],
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        'gatsby-transformer-sharp',
        {
            resolve: `gatsby-plugin-sharp`,
            options: {
                icon: `src/images/apple-icon-1800x180.png`,
            },
        },
        'gatsby-plugin-react-helmet',
        {
            resolve: "gatsby-plugin-react-svg",
            options: {
                rule: {
                    include: [/images\/.*\.svg/, /components\/elements\/*\/.*\.svg/],
                },
            },
        },
    ],
};
