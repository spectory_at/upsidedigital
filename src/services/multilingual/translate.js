import {I18n} from "i18n-js";
import detectBrowserLanguage from 'detect-browser-language'
import { navigate } from "gatsby-link";

const i18n = new I18n({
    "de_DE": {
      hello: "Hallo!",
    },
    "en_US": {
      hello: "Hello!",
    },
  });

export const translationGetters = {
    de: () => require("./translations/de.json"),
    en: () => require("./translations/en.json"),
}

export const getLocaleFromLang = (lang) => {
    let langs = {
        'de': 'de_DE',
        'en': 'en_US'
    }

    let locale = langs[lang];

    return locale;
}

export const t = (k, lang = null, a = {}) => {

    if (lang !== null) {
        return translationGetters[lang]()[k]
    }
    return i18n.t(k, a);

}

export const tStatic = (k, lang = null, a = {}) => {

    if (lang === null) return '';
    return translationGetters[lang]()[k]

}

export const getLan = () => {

    return (i18n.locale);

}

export const setTranslationConfig = (languageTag) => {

    const fbLanguage = 'en';

    if (translationGetters[languageTag] !== undefined) {
        i18n.locale = languageTag;
        i18n.translations = { [languageTag]: translationGetters[languageTag]() };
    } else {
        i18n.locale = fbLanguage;
        i18n.translations = { [fbLanguage]: translationGetters[fbLanguage]() };
    }

}

export const setBrowserLanguage = (slug) => {

    let lan = detectBrowserLanguage().split('-')[0];
    let storedLan = localStorage.getItem("lan");
    
    if (storedLan === null) {
        setTranslationConfig(lan);
        localStorage.setItem('lan', lan);

        let languageBase = '';

        if(lan === 'en'){
            languageBase = '/en'
        }

        navigate(languageBase+slug);


    } else {
        lan = storedLan;
    }

    return lan

}