import React, { useState } from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import * as blogStyles from '../components/elements/BlogPosts/blogposts.module.scss';

import Layout from "../components/layout"
import { ParallaxProvider } from "react-scroll-parallax";
import Hero from "../components/elements/Hero";
import parse from 'html-react-parser';
import { SplitHeadlines } from "../components/elements/BlogPosts";
import SmallPost from "../components/elements/BlogPosts/small";
import Button from "../components/elements/Button";

const PostPage = props => {

    const posts_unfiltered = useStaticQuery(graphql`
    {
        allWpPost(sort: {order: DESC, fields: date}) {
          nodes {
            id
            title
            content
            dateDe: date(formatString: "DD. MMMM YYYY", locale: "de_De")
            dateEn: date(formatString: "DD. MMMM YYYY", locale: "en_Us")
            featuredImage {
              node {
                sourceUrl
              }
            }
            locale {
                locale
            }
            excerpt
            uri
          }
        }
      }
    `);

    let path = props.pageContext.path;
    const data = props.pageContext.data.node;
    const lang = props.pageContext.lang.split('_')[0];
    const readingLength = Math.floor(data.content.split(/\s+/).filter(word => word.length > 0).length / 230);

    let posts_filtered = posts_unfiltered.allWpPost.nodes.filter((p) => (p.locale.locale.startsWith(lang) && p.id !== data.id));
    const linkBase = `/${lang === 'en' ? 'en/' : ''}blog`;
    const [shownSmall, setShownSmall] = useState(3);

    console.log(posts_filtered);

    const infos = `${lang.startsWith('en') ? data.dateEn : data.dateDe} | ${lang == 'en' ? 'Reading time:' : 'Lesezeit'} ${readingLength} min`;

    const showMore = () => {
        if (posts_filtered.length - shownSmall <= 3) {
            setShownSmall(posts_filtered.length);
        } else {
            setShownSmall(prev => prev + 3);
        }
    }

    const heroChildren = [
        {
            id: "row",
            classes: ["transparent", "normal", "boxed", "", ""],
            style: {},
            "sidemenu_id": "",
            "sidemenu_title": "",
            "columns": "2",
            "column_sizes": {
                "col1": "6",
                "col2": "6"
            },
            "children": [
                {
                    "children": [
                        {
                            "id": "spacer",
                            "style": {
                                "height": "0px",
                                "heightDesktop": "",
                                "heightTablet": "",
                                "heightMobile": "100px"
                            }
                        },
                        {
                            "id": "text",
                            "classes": ["straight"],
                            "style": {
                                "margin_top": "",
                                "margin_bottom": ""
                            },
                            "text": infos,
                            "animate": true
                        },
                        {
                            "id": "spacer",
                            "style": {
                                "height": "20px",
                                "heightDesktop": "",
                                "heightTablet": "",
                                "heightMobile": ""
                            }
                        },
                        {
                            "id": "headline",
                            "style": {
                                "stroke_alignment": "left",
                                "margin_top": "0px",
                                "margin_bottom": "0px"
                            },
                            "type": "h1",
                            "text": data.title,
                            "subheadline": "",
                            "animate": true
                        },
                        {
                            "id": "spacer",
                            "style": {
                                "height": "50px",
                                "heightDesktop": "",
                                "heightTablet": "",
                                "heightMobile": "40px"
                            }
                        },
                        {
                            "id": "text",
                            "classes": ["classic"],
                            "style": {
                                "margin_top": "",
                                "margin_bottom": ""
                            },
                            "text": data.excerpt,
                            "animate": true
                        },
                        {
                            "id": "spacer",
                            "style": {
                                "height": "70px",
                                "heightDesktop": "",
                                "heightTablet": "",
                                "heightMobile": "0px"
                            }
                        },
                        {
                            "id": "button",
                            "title": lang.startsWith('en') ? 'Read more' : 'Mehr lesen',
                            "type": "secondary",
                            "destination": '#article'
                        }
                    ]
                },
                {
                    "children": [
                        {
                            "id": "single_image",
                            "style": { width: '80%', marginLeft: 50 },
                            "url": data.featuredImage?.node.mediaItemUrl,
                            "imageStyle": "rounded",
                            "animate": true
                        }
                    ]
                }
            ]
        }
    ];

    return (
        <ParallaxProvider>
            <Layout
                whiteHeader={true}
                lang={lang}
                path={path}
                hideMenu={false}
                title={data.title}
                seoMetaTags={{
                    sm: {
                        title: data.smTitle,
                        description: data.smDescription,
                        image: data.featuredImage?.node.mediaItemUrl
                    },
                    google: {
                        title: data.googleTitle,
                        description: data.googleDescription,
                    }
                }}
            >
                <Hero children={heroChildren} />
                <div id="article" className="container row">
                    <div className="col9">
                        <div className="blog-content">
                            {parse(data.content)}
                        </div>
                    </div>
                    <div className="col3">

                    </div>
                </div>
                <div className="container row">
                    <div className="col12">
                        <div style={{marginBottom: 60}}>
                            {posts_filtered.length > 0 ?
                                <>
                                    {<SplitHeadlines lang={lang} />}
                                    <div className={blogStyles.smallPostsWrapper}>
                                        {posts_filtered.slice(0, shownSmall).map((p) => <SmallPost {...p} link={`${linkBase}${p.uri}`} />)}
                                    </div>
                                    {shownSmall < posts_filtered.length ?
                                        <div className={blogStyles.buttonWrapper}><Button onClick={showMore} type="primary" title={lang.startsWith('en') ? "Show more" : "Mehr anzeigen"} /></div>
                                        : undefined}
                                </>
                                : undefined}
                        </div>
                    </div>
                </div>
            </Layout>
        </ParallaxProvider>
    )
}

export default PostPage