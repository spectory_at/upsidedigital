import * as React from "react"
import {Link, graphql} from "gatsby"

import Layout from "../components/layout"
import renderElements from "../components/elements";
import {ParallaxProvider} from "react-scroll-parallax";

export const query = graphql`
  query($id: String!) {
    wpJob(
        id: { eq: $id },
        ) {
      slug
      title
      localizedWpmlUrl
      renderContent
    }
  }
`

const JobPage = props => {

    const lang = props.pageContext.lang.split('_')[0];

    return (
        <ParallaxProvider>
            <Layout
                contentObjects={JSON.parse(props.data.wpJob.renderContent)}
                lang={lang}
                path={props.pageContext.path}
                title={props.data.wpJob.title}
            >
                {renderElements(props.data.wpJob.renderContent, lang)}
            </Layout>
        </ParallaxProvider>
    )
}

export default JobPage