import * as React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import renderElements from "../components/elements";
import { ParallaxProvider } from "react-scroll-parallax";

export const query = graphql`
  query($slug: String!, $lang: String!) {
    wpPage(
        slug: { eq: $slug },
        locale:{locale: {eq: $lang}}
        ) {
        id
      slug
      title
      renderContent
      hideMenu
      smTitle
      smDescription
      googleTitle
      googleDescription
      featuredImage {
          node {
            mediaItemUrl
          }
      }
    }
  }
`

const SecondPage = props => {

    let path = '/' + props.pageContext.slug;

    if (props.pageContext.isFrontPage) {
        path = '/';
    }

    const lang = props.pageContext.lang.split('_')[0];

    if (props.data.wpPage === null) return null;

    return (
        <ParallaxProvider>
            <Layout
                contentObjects={JSON.parse(props.data.wpPage.renderContent)}
                lang={lang}
                path={path}
                hideMenu={props.data.wpPage.hideMenu}
                title={path !== '/' && path !== '/en' ? props.data.wpPage.title : ''}
                seoMetaTags={{
                    sm: {
                        title: props.data.wpPage.smTitle,
                        description: props.data.wpPage.smDescription,
                        image: props.data.wpPage.featuredImage?.node.mediaItemUrl
                    },
                    google: {
                        title: props.data.wpPage.googleTitle,
                        description: props.data.wpPage.googleDescription,
                    }
                }}
            >
                {renderElements(props.data.wpPage.renderContent, lang)}
            </Layout>
        </ParallaxProvider>
    )
}

export default SecondPage