import * as React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import renderElements from "../components/elements";
import { ParallaxProvider } from "react-scroll-parallax";

export const query = graphql`
  query($id: String!) {
    wpReferenz(
        id: { eq: $id }
        ) {
      uri
      title
      renderContent
    }
  }
`

const ReferencePage = props => {

    if (props.data.wpReferenz === null) return null;
    let path = props.data.wpReferenz.uri;

    const lang = props.pageContext.lang.split('_')[0];


    return (
        <ParallaxProvider>
            <Layout
                contentObjects={JSON.parse(props.data.wpReferenz.renderContent)}
                lang={lang}
                path={path}
                hideMenu={false}
                title={props.data.wpReferenz.title}
            >
                {renderElements(props.data.wpReferenz.renderContent, lang)}
            </Layout>
        </ParallaxProvider>
    )
}

export default ReferencePage