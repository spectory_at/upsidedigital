import React, { useEffect, useRef, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";

import * as styles from "../components/elements/Hero/styles.module.scss";

import NewShape from "../components/elements/Hero/newShape";

const Hero = (props) => {
    const wrapper = useRef();
    const [showAnimation, setShowAnimation] = useState(false);
    const [largerThanScreen, setLargerThanScreen] = useState(false);

    const setContainerHeight = (height) => {
        wrapper.current.style.height = height + "px";

        if (wrapper.current.getElementsByClassName("container") !== undefined) {
            // alert("wrapper" + JSON.stringify(wrapper))
            wrapper.current.getElementsByClassName("container")[0].style.height =
                wrapper.current.clientHeight + 50 + "px";
        }

        if (
            wrapper.current.clientHeight > document.documentElement.clientHeight
        ) {
            setLargerThanScreen(true);

        }
    };

    return (
        <>
            <div
                ref={wrapper}
                className={`${styles.hero} ${styles[props.type]} ${largerThanScreen && "largerThanScreen"
                    }`}
            >

                <NewShape shapeType={'first'} wrapper={wrapper} className={styles.shape} />
            </div>
        </>
    );

};

function mapStateToProps(state) {
    return {
        animationPointsPositionOnPage:
            state.app.animationPointsPositionOnPage || undefined,
    };
}

export default connect(mapStateToProps, null)(Hero);
