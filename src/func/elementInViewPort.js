const elementInViewport = (el) => {

    if (el !== null) {

        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;

        while (el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        if (height > document.documentElement.clientHeight) {

            let threequarterScreenHeight = document.documentElement.clientHeight / 4 * 3;
            return window.pageYOffset > top - threequarterScreenHeight;

        } else {
            return (
                top >= window.pageYOffset && // IF Element top is still in Viewport
                left >= window.pageXOffset && // IF Element left is still in Viewport
                (top + height) <= (window.pageYOffset + document.documentElement.clientHeight) &&
                (left + width) <= (window.pageXOffset + window.innerWidth)
            );
        }



    }

}

export default elementInViewport;