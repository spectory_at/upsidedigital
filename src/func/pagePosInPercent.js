function getVerticalScrollPercentage(elm = document.body) {
	var p = elm.parentNode;
	let pos =
		((elm.scrollTop || p.scrollTop) / (p.scrollHeight - p.clientHeight)) * 100;
	return Math.round(pos);
}

export default getVerticalScrollPercentage;
