import * as React from "react";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import Header from "../components/sections/header";
import Footer from "../components/sections/footer";
import MobileMenu from "./sections/MobileMenu";
import { setBrowserLanguage } from "../services/multilingual/translate";
import { clearPointData } from "../state/app";

import "../scss/fonts.scss";
import "../scss/layout.scss";
import "../scss/animations.scss";
import "animate.css/animate.css";
import "../scss/common.scss";
import ScrollLineBar from "./elements/ScrollLine/index";
import windowSize from "../func/windowSize";
import Burger from "./elements/Burger/index";
import Seo from "./sections/seo";
import CookieNotice from "./elements/CookieNotice";
import { capiPageView } from "../services/capi";

const Layout = ({ whiteHeader, children, lang, path, contentObjects, hideMenu, title, seoMetaTags}) => {
    const [menu, setMenu] = useState(false);
    const [mobile, setMobile] = useState(false);
    const dispatch = useDispatch();

    const theMobile = () => {
        if (windowSize() <= 992) {
            setMobile(true);
        } else {
            setMobile(false);
        }
    };

    useEffect(() => {
        capiPageView();
    })

    let activeDefault = true;
    let white = false;

    if (contentObjects !== undefined) {
        contentObjects.forEach((element) => {
            if (
                element !== null &&
                (element.id === "start_header" || element.id === "hero")
            )
                activeDefault = false;

            if (element !== null && element.id === "hero" && element.type === "third")
                white = true;
        });
    }

    if(whiteHeader) {
        activeDefault = false;
    }


    useEffect(() => {

        dispatch(clearPointData());

        theMobile();
        setBrowserLanguage(path);
        window.addEventListener("resize", theMobile);
        return () => {
            window.removeEventListener("resize", theMobile);
        };
    }, []);

    return (
        <>
            <Seo title={title} seoMetaTags={seoMetaTags} />
            {mobile && <ScrollLineBar />}
            {menu && (
                <MobileMenu lang={lang} menu={menu} setMenu={setMenu} path={path} isMobile={mobile} />
            )}
            <Header
                menu={menu}
                setMenu={setMenu}
                hideMenu={hideMenu}
                lang={lang}
                path={path}
                activeDefault={activeDefault}
                white={white}
            />
            <CookieNotice lang={lang} />
            <main>{children}</main>
            <Footer lang={lang} path={path} />
        </>
    );
};

export default Layout;
