import React from "react";
import * as styles from "./styles.module.scss";

import Profile1 from '../../../images/mitarbeiter_1.png';
import Profile2 from '../../../images/mitarbeiter_2.png';
import Profile3 from '../../../images/mitarbeiter_3.png';

const Career = ({ url, title }) => {
	return (
		<div className={styles.career}>
			<svg
				className={styles.icon}
				xmlns="http://www.w3.org/2000/svg"
				width="60"
				height="52"
				viewBox="0 0 76 68"
			>
				<g id="Group_6" data-name="Group 6" transform="translate(-362 -4100)">
					<g
						id="Rectangle_54"
						data-name="Rectangle 54"
						transform="translate(362 4116)"
						fill="none"
						stroke="#fe5000"
						strokeLinecap="round"
						strokeLinejoin="round"
						strokeWidth="5"
					>
						<rect width="76" height="52" rx="10" stroke="none" />
						<rect x="2.5" y="2.5" width="71" height="47" rx="7.5" fill="none" />
					</g>
					<g
						id="Rectangle_55"
						data-name="Rectangle 55"
						transform="translate(384 4100)"
						fill="none"
						stroke="#fe5000"
						strokeLinecap="round"
						strokeLinejoin="round"
						strokeWidth="5"
					>
						<rect width="33" height="68" rx="10" stroke="none" />
						<rect x="2.5" y="2.5" width="28" height="63" rx="7.5" fill="none" />
					</g>
				</g>
			</svg>
			<div className={styles.title}>{title}</div>
			<div className={styles.items}>
				<img src={Profile1} className={styles.profile} />
				<img src={Profile2} className={styles.profile} />
				<img src={Profile3} className={styles.profile} />
				<a href={url}>
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="25.886"
						height="25.886"
						viewBox="0 0 25.886 25.886"
					>
						<g
							id="Group_227"
							data-name="Group 227"
							transform="translate(-659.562 -6615.928)"
						>
							<line
								id="Line_37"
								data-name="Line 37"
								y2="21.886"
								transform="translate(672.504 6617.928)"
								fill="none"
								stroke="#fafafa"
								stroke-linecap="round"
								stroke-width="4"
							/>
							<line
								id="Line_38"
								data-name="Line 38"
								y2="21.886"
								transform="translate(683.447 6628.871) rotate(90)"
								fill="none"
								stroke="#fafafa"
								stroke-linecap="round"
								stroke-width="4"
							/>
						</g>
					</svg>
				</a>
			</div>
		</div>
	);
};

export default Career;
