import React, { useEffect, useState } from "react";
import renderElements from "..";
import windowSize from "../../../func/windowSize";

const DisplayScreen = ({ children, display, lang }) => {
    const [show, setShow] = useState(false);
    const displayElement = () => {
        if (display == "both") {
            setShow(true);
        } else if (display == "desktop" && windowSize() >= 992) {
            setShow(true);
        } else if (display == "mobile" && windowSize() < 992) {
            setShow(true);
        } else {
            setShow(false);
        }
    };

    useEffect(() => {
        displayElement();
        window.addEventListener("resize", displayElement);
        return () => {
            window.removeEventListener("resize", displayElement);
        };
    }, []);

    return <>{show && <div>{renderElements(children, lang)}</div>}</>;
};

export default DisplayScreen;
