import React, { useEffect, useState } from "react";
import Button from "../Button";
import * as styles from "./styles.module.scss";

const Image = (props) => {

    const [responsiveStyles, setResponsiveStyles] = useState({});

    const calcStyles = (e) => {
        const windowWidth = document.documentElement.clientWidth;

        let styles = {
            marginTop: props.style.marginTop || 0,
            marginBottom: props.style.marginBottom || 0,
            marginLeft: props.style.marginLeft || 0,
            marginRight: props.style.marginRight || 0,
            width: props.style.width,
            display: "block"
        };

        // if (windowWidth <= 1100) {
        //     styles = {
        //         marginTop: props.style.marginTop,
        //         marginBottom: props.style.marginBottom,
        //         marginLeft: props.style.marginLeft,
        //         marginRight: props.style.marginRight,
        //         width: props.style.width
        //     };
        // }

        if (windowWidth <= 992) {

            if (props.style.marginTopTablet !== null) {
                styles.marginTop = props.style.marginTopTablet;
            }
            if (props.style.marginBottomTablet !== null) {
                styles.marginBottom = props.style.marginBottomTablet;
            }
            if (props.style.marginLeftTablet !== null) {
                styles.marginLeft = props.style.marginLeftTablet;
            }
            if (props.style.marginRightTablet !== null) {
                styles.marginRight = props.style.marginRightTablet;
            }
            if (props.style.widthTablet !== '') {
                styles.width = props.style.widthTablet;
            }
        }

        if (windowWidth <= 768) {
            if (props.style.marginTopMobile !== null) {
                styles.marginTop = props.style.marginTopMobile;

            }
            if (props.style.marginBottomMobile !== null) {
                styles.marginBottom = props.style.marginBottomMobile;
            }
            if (props.style.marginLeftMobile !== null) {
                styles.marginLeft = props.style.marginLeftMobile;
            }
            if (props.style.marginRightMobile !== null) {
                styles.marginRight = props.style.marginRightMobile;
            }
            if (props.style.widthMobile !== '') {
                styles.width = props.style.widthMobile;
            }
        }

        if (props.position == "center") {
            styles.marginLeft = "auto";
            styles.marginRight = "auto";
        } else if (props.position == "right") {
            styles.float = "right";
        }

        setResponsiveStyles(styles);

    }

    useEffect(() => {

        calcStyles();
        window.addEventListener('resize', calcStyles);

        return () => {
            window.addEventListener('resize', calcStyles);
        }
    }, [])

    let image = <img
        src={props.url}
        // className={`image ${props.imageStyle == "rounded" && styles.rounded}`}
        style={responsiveStyles}
        height={responsiveStyles.width === 'auto' ? props.height : 'auto'}
    />;

    if (props.imageStyle == "rounded") {

        image = <div className={`image ${styles.rounded}`}>{image}</div>;
    }

    return (
        <>
            {image}
        </>
    );
};

export default Image;
