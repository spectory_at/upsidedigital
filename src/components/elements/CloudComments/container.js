import React, { useEffect, useRef, useState } from "react";
import renderElements from "../index";
import { v4 as uuidv4 } from "uuid";
import elementInViewport from "../../../func/elementInViewPort";
import animateCSS from "../../../func/animateCSS";
import * as styles from "./styles.module.scss";
import CloudCommentsItem from "./item";

const CloudCommentsContainer = ({ children, style, lang, key }) => {
	const ref = useRef();
	const [items, setItems] = useState([]);
	const [animatedItems, setAnimatedItems] = useState([]);

	if (style.type == "second") {
		children.map((e) => {
			e.classes = styles.secondStyle;
			return e;
		});
	}

	let sProps = { display: "flex", flexDirection: "column" };
	if (style.position == "center") {
		sProps.alignItems = "center";
	} else if (style.position == "right") {
		sProps.alignItems = "flex-end";
	}

	return (
		<div className={styles.container} style={sProps}>
			{children.map((item, key) => {
				return <CloudCommentsItem {...item} key={key} />;
			})}
		</div>
	);
};

export default CloudCommentsContainer;
