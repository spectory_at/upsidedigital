import React, { useEffect, useRef } from "react";
import animateCSS from "../../../func/animateCSS";
import elementInViewport from "../../../func/elementInViewPort";
import * as styles from "./styles.module.scss";

const CloudCommentsItem = ({ text, image_url, classes, animate }) => {

	const ref = useRef();

	useEffect(() => {
		let animatedLocal = false;

		const check = () => {
			if (ref.current) {
				if (elementInViewport(ref.current)) {
					if (!animatedLocal) {
						animatedLocal = true;

						ref.current.classList.add(styles.show);
						animateCSS(ref.current, "bounceInUp").then((message) => {});
					}
				}
			}
		};
		check(ref);

		document.addEventListener(
			"scroll",
			() => {
				check(ref);
			},
			[],
		);
	}, []);

	return (
		<div ref={ref} className={`cloud_comment_item ${styles.item} ${classes}`}>
			<div className={styles.text}>{text}</div>
			<img width="67px" height="67px" src={image_url} />
		</div>
	);
};

export default CloudCommentsItem;
