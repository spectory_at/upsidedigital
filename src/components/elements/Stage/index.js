import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";

import { addNewPointData } from "../../../state/app";

import getPosition from "../../../func/elementPosition";
import renderElements from "../index";

import * as styles from "./styles.module.scss";

const Stage = ({ children, image_url, anchor_type }) => {
    const ref = useRef();
    const dispatch = useDispatch();

    const [top, setTop] = useState(50);

    useEffect(() => {
        
        setTimeout(() => {

            if (ref.current !== null) {

                let headline = ref.current.getElementsByTagName('h2')[0]
                let _top = 0;

                if (headline) {
                    _top = headline.offsetTop + getPosition(ref.current) + (headline.offsetHeight / 2) - 40;
                }

                let image = ref.current.getElementsByClassName('image')[0]
                if (anchor_type === 'image' && image !== undefined) {

                    _top = image.offsetTop + getPosition(ref.current) + (image.offsetHeight / 2) - 40;

                }


                dispatch(
                    addNewPointData({
                        image: image_url,
                        position: _top,
                    }),
                );

            }

            
        }, 1000)

    }, []);
    return (
        <div className={`${styles.container} stageContainer`} ref={ref}>
            {renderElements(children)}

        </div>
    );
};

export default Stage;
