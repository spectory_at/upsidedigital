import React from "react";
import * as styles from "./styles.module.scss";
const Burger = ({ state, onClick, active, white }) => {
	return (
		<div
			onClick={onClick}
			className={`${styles.Burger} ${state ? styles.clicked : ""} ${white ? styles.white : ""} ${
				active ? styles.active : styles.inactive
			}`}
		>
			<div className={styles.line}></div>
			<div className={styles.line}></div>
			<div className={styles.line}></div>
		</div>
	);
};

export default Burger;
