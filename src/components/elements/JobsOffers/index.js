import * as React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import * as styles from "./joboffers.module.scss";

import ArrowIcon from "../../../images/arrow-right.svg";
import { getLocaleFromLang, tStatic } from "../../../services/multilingual/translate";

const JobOffers = props => {
    const jobsQuery = useStaticQuery(graphql`
        {
            allWpJob{
                edges {
                    node {
                        title
                        location
                        department
                        localizedWpmlUrl
                        locale {
                            locale
                        }
                    }
                }
            }
        }
    `);


    const jobs = jobsQuery.allWpJob.edges;

    const renderJobList = () => {

        return jobs.map(job => {
            job = job.node;

            if (job.locale.locale !== getLocaleFromLang(props.lang)) return;

            return <Link className={styles.row} to={`${job.localizedWpmlUrl}`}>
                <ArrowIcon className={styles.arrowCol} />
                <div className={styles.col}>{job.title}</div>
                {/* <div className={styles.col}>{job.department}</div> */}
                <div className={styles.col}>{job.location}</div>
            </Link>
        })

    }

    return (
        jobs.length > 0 ?
            <div className={styles.container}>
                {renderJobList()}
            </div>
            : <><br/>{tStatic("no_open_jobs", props.lang)}</>
    )



}

export default JobOffers