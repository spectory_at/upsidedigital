import React from 'react';
import * as styles from "./styles.module.scss"
import Button from "../Button";
import parse from 'html-react-parser';

import TwitterIcon from "../../../images/twitter.svg";
import FacebookIcon from "../../../images/facebook.svg";
import AddressIcon from "../../../images/address.svg";
import PhoneIcon from "../../../images/phone.svg";
import EmailIcon from "../../../images/email.svg";
import LinkedInIcon from "../../../images/linkedin.svg";


// import TwitterIconWhite from "../../../images/twitterWhite.svg";
// import FacebookIconWhite from "../../../images/facebookWhite.svg";
import AddressIconWhite from "../../../images/addressWhite.svg";
import PhoneIconWhite from "../../../images/phoneWhite.svg";
import EmailIconWhite from "../../../images/emailWhite.svg";
// import LinkedInIconWhite from "../../../images/linkedinWhite.svg";

import { Link } from 'gatsby';

const ContactCard = (props) => {

    if (props.card) {
        return (
            <div className={`${styles.container} ${styles.card}`}>
                <h2>Upside Digital GmbH</h2>
                <div className={styles.contentWrapper}>
                    <div className={styles.containerLeft}>
                        <div className={styles.textContainer} style={{ paddingBottom: 20 }}>
                            <div className={styles.iconContainer}>
                                <AddressIcon
                                    style={{
                                        width: 20
                                    }}
                                />
                            </div>
                            <div className={styles.contentContainer}>
                                {parse(props.address)}
                            </div>
                        </div>
                        <div className={styles.textContainer}>
                            <div className={styles.iconContainer}>
                                <PhoneIcon
                                    style={{
                                        width: 20
                                    }}
                                />
                            </div>
                            <div className={styles.contentContainer}>
                                <a style={{ display: 'block' }} href={`tel:${props.phone}`}>{parse(props.phone)}</a>
                                <a href={`tel:${props.phone_second}`}>{parse(props.phone_second)}</a>
                            </div>
                        </div>
                        <div className={styles.textContainer} style={{ marginTop: 10 }}>
                            <div className={styles.iconContainer}>
                                <EmailIcon
                                    style={{
                                        height: 16
                                    }}
                                />
                            </div>
                            <div className={styles.contentContainer}>
                                <a href={`mailto:${props.email}`}>{parse(props.email)}</a>
                            </div>
                        </div>
                    </div>
                    <div className={styles.containerRight}>
                        <Link to="https://www.facebook.com/UPSIDE.media" target="_blank"><FacebookIcon /></Link>
                        <Link to="https://twitter.com/my_upside" target="_blank"><TwitterIcon /></Link>
                        <a target="_blank" href="https://www.linkedin.com/company/upside-digital-gmbh" target="_blank"><LinkedInIcon /></a>

                    </div>
                </div>


            </div>
        );
    }


    return (
        <div className={`${styles.container}`}>
            <div className={styles.contentWrapper}>
                <div className={styles.containerLeft}>
                    <div className={styles.textContainer}>
                        <div className={styles.iconContainer}>
                            <AddressIconWhite
                                style={{
                                    width: 20
                                }}
                            />
                        </div>
                        <div className={styles.contentContainer}>
                            {parse(props.address)}
                        </div>
                    </div>
                    <div className={styles.textContainer}>
                        <div className={styles.iconContainer}>
                            <PhoneIconWhite
                                style={{
                                    width: 20
                                }}
                            />
                        </div>
                        <div className={styles.contentContainer}>
                            <a style={{ display: 'block' }} href={`tel:${props.phone}`}>{parse(props.phone)}</a>
                            <a href={`tel:${props.phone_second}`}>{parse(props.phone_second)}</a>
                        </div>
                    </div>
                    <div className={styles.textContainer}>
                        <div className={styles.iconContainer}>
                            <EmailIconWhite
                                style={{
                                    width: 26
                                }}
                            />
                        </div>
                        <div className={styles.contentContainer}>
                            <a href={`mailto:${props.email}`}>{parse(props.email)}</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );

};

export default ContactCard;
