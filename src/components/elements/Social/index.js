import React from "react";
import {
	FaFacebookSquare,
	FaGooglePlusG,
	FaLinkedin,
	FaRss,
	FaTwitter,
} from "react-icons/fa";
import * as styles from "./styles.module.scss";

const Social = () => {
	return (
		<ul className={`${styles.social} social`}>
			<li>
				<a target="_blank" href="https://www.facebook.com/UPSIDE.media">
					<FaFacebookSquare />
				</a>
			</li>
			<li>
				<a target="_blank" href="https://twitter.com/my_upside">
					<FaTwitter style={{ width: "20px" }} />
				</a>
			</li>
			<li>
				<a
					target="_blank"
					href="https://www.linkedin.com/company/upside-digital-gmbh"
				>
					<FaLinkedin />
				</a>
			</li>
		</ul>
	);
};

export default Social;
