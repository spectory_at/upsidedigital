import React, { useEffect, useState } from "react";
import renderElements from "..";

import * as styles from './styles.module.scss';

const AlignItem = (props) => {

    let align = 'flex-start';

	if (props.style.justifyContent == "left") {
		align = "flex-start";
	} else if (props.style.justifyContent == "right") {
		align = "flex-end";
	} else if (props.style.justifyContent == "center") {
		align = "center";
	}
	const style = {
		display: "flex",
		justifyContent: align,
		flexDirection: props.style.flexDirection ? props.style.flexDirection : "row",
		alignItems: props.style.alignItems ? props.style.alignItems : "flex-start",
		height: "100%",
        marginTop: props.style.marginTop,
        marginBottom: props.style.marginBottom,
	};
	return <>{<div className={styles.container} style={style}>{renderElements(props.children, props.lang)}</div>}</>;
};

export default AlignItem;
