import React, { useEffect, useRef, useState } from "react";

const StartHeroMobileShape = ({ wrapper, className, style, setParentHeight }) => {


    const [coordinates, setCoordinates] = useState('');
    const [offset, setOffset] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);
    const [windowHeight, setWindowHeight] = useState(0);
    const [preHeight, setPreHeight] = useState(0);
    const [reloading, setReloading] = useState(false);


    const onResize = () => {

        // setWindowWidth(wrapper.current.clientWidth);
        // setCoordinates(`M0 0H${width}V${bottom}S${(width / 4 * 3)},${bottom - 50},${width / 2},${bottom}S${(width / 7)},${bottom + 30},${0},${bottom - 50}Z`);

        if (setParentHeight !== undefined && wrapper.current !== undefined) {

            let width = wrapper.current.clientWidth;

            let bottom = 0;
            let offsetRight = 100;

            let height = document.documentElement.clientHeight + 330;

            bottom = height;
            setWindowHeight(height + 100);

            if (setParentHeight !== undefined) {

                setParentHeight(document.documentElement.clientHeight);

            }


            if(wrapper.current.clientWidth < 768){
                offsetRight = 70;
            }


            setWindowWidth(wrapper.current.clientWidth);
            setCoordinates(`M0 0V${bottom}S${(width / 4 * 2)},${bottom + 50},${width},${bottom - offsetRight}V0Z`);

        }

    }

    const reload = () => {
        setReloading(true)
        setTimeout(() => {
            setReloading(false)
        }, 10)
    }

    useEffect(() => {

        if (wrapper.current !== undefined) {
            setPreHeight(wrapper.current.clientHeight)
        }

        onResize();
        reload();

        window.addEventListener("resize", () => onResize());

        return () => {
            window.removeEventListener("resize", onResize);
        }


    }, [])

    if(reloading) return null;

    return (
        <>
            <svg className={className} width={windowWidth} height={windowHeight} viewBox={`0 0 ${windowWidth} ${windowHeight}`}>

                <defs>
                    <linearGradient id="start-hero-mobile-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                        <stop offset="0" stop-color="#fb8f22" />
                        <stop offset="0.317" stop-color="#fe5000" />
                        <stop offset="0.455" stop-color="#fe5000" />
                        <stop offset="0.667" stop-color="#fc7815" />
                        <stop offset="1" stop-color="#fb8f22" />
                    </linearGradient>
                    <filter id="start-hero-mobile-shadow" x="0" y="0" width={windowWidth} height={windowHeight} filterUnits="userSpaceOnUse">
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood flood-opacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                    </filter>
                </defs>
                <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#start-hero-mobile-shadow)">
                    <path id="start-hero-mobile" data-name="Start Hero Mobile" d={coordinates} fill="url(#start-hero-mobile-gradient)" />
                </g>
            </svg>

        </>
    );

};

export default StartHeroMobileShape;