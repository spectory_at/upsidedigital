import React, { useEffect, useRef, useState } from 'react';
import * as styles from "./starthero.module.scss";
import { TweenMax } from "gsap/all";
import { Sine } from 'gsap/gsap-core';
import { Linear } from 'gsap/gsap-core';

const SvgComponent = ({ wrapper }) => {

    const [splineCoords, setSplineCoords] = useState("");
    const [dotsCoords, setDotsCoords] = useState([]);
    const [isAnimatingLoop, setIsAnimatingLoop] = useState(true);
    const pixelsPerDot = 100;
    const dotRadius = 7;

    const graph = useRef();

    const svg = () => {

        if (wrapper.current === null) return null;

        let height = wrapper.current.clientHeight;
        let width = wrapper.current.clientWidth

        if(width < 1320) height = height / 2;


        return <svg width={wrapper.current.clientWidth} height={height} style={{zIndex: -1}}
            viewBox={`0 -${height / 2} ${wrapper.current.clientWidth} ${height}`}
        >
            <path d={splineCoords} ref={graph} className={styles.path} />
            {renderDots()}
        </svg>

    }

    const renderDots = () => {
        return dotsCoords.map((dotCoord, key) => {
            return <circle key={key} cx={dotCoord.x} cy={dotCoord.y} r={dotRadius} className={styles.dot} />
        })
    }

    const getAmplitude = () => {
        let positive = Math.round(Math.random()) === 0;

        if(wrapper.current === null) return document.documentElement.clientHeight * 0.3 ;

        const maxAmplitude = wrapper.current.clientHeight * 0.2;

        let amplitude = (Math.random() * maxAmplitude);

        if (!positive) {
            amplitude = amplitude * -1;
        }

        return amplitude;
    }

    const getDots = () => {
        let amount = Math.round(document.documentElement.clientWidth / pixelsPerDot);
        let width = document.documentElement.clientWidth / amount;

        amount = amount + 2;

        return {
            amount: amount,
            width: width
        }
    }

    const getCoords = (prevCoordsLine, prevCoordsDots = null) => {

        let dots = getDots();

        let pointsLine = [];
        let endPointsLine = [];
        let pointsDots = [];
        let endPointsDots = [];
        let sineCoords = [];

        for (let i = 0; i < dots.amount; i++) {

            let x = (dots.width * i) - (dots.width / 2);

            let point = prevCoordsLine[i * 2 + 1];
            let pointDot = prevCoordsLine[i * 2 + 1];

            let endPoint = getAmplitude();

            if (point === undefined) {
                point = getAmplitude();
            }

            // X-Achsis
            pointsLine.push(x);
            endPointsLine.push(x);
            endPointsDots.push(x);
            sineCoords.push(x);

            // Y-Achsis
            pointsLine.push(point);
            if (prevCoordsDots !== null) {
                pointsDots.push(prevCoordsDots[i * 2]);
                pointsDots.push(prevCoordsDots[i * 2 + 1]);
            } else {
                pointsDots.push(x);
                pointsDots.push(pointDot * -1);
            }
            endPointsLine.push(endPoint);
            endPointsDots.push(endPoint * -1);

            if (i % 2) {
                sineCoords.push(document.documentElement.clientHeight * 0.1);
            } else {
                sineCoords.push(document.documentElement.clientHeight * -0.1);
            }

        }


        // First and last to stay
        pointsLine[1] = 0;
        pointsLine[pointsLine.length - 1] = 0;

        endPointsLine[1] = 0;
        endPointsLine[pointsLine.length - 1] = 0;

        return {
            line: {
                start: pointsLine,
                end: endPointsLine,
            },
            dots: {
                start: pointsDots,
                end: endPointsDots,
            },
            sine: sineCoords
        }

    }

    useEffect(() => {

        let tweenLine = null;
        let tweenDots = null;

        let tweenLineTransition = null;
        let tweenDotsTransition = null;

        let tweenDotsSine = null;

        let animationStarted = false;

        let pointsCacheLine = [];
        let pointsCacheDots = [];
        let sineCoords = [];


        const animate = (prevCoordsLine = [], prevCoordsDots = null) => {

            const coords = getCoords(prevCoordsLine, prevCoordsDots);

            let endPoitsLine = coords.line.end;
            let endPoitsDots = coords.dots.end;
            let pointsLine = coords.line.start;
            let pointsDots = coords.dots.start;

            let tension = 1;

            const cardinalSpline = (data) => {

                let size = data.length;
                let last = size - 4;

                let path = `M ${data[0]} 0 C`;

                for (let i = 0; i < size - 2; i += 2) {

                    let x0 = i ? data[i - 2] : data[0];
                    let y0 = i ? data[i - 1] : data[1];

                    let x1 = data[i + 0];
                    let y1 = data[i + 1];

                    let x2 = data[i + 2];
                    let y2 = data[i + 3];

                    let x3 = i !== last ? data[i + 4] : x2;
                    let y3 = i !== last ? data[i + 5] : y2;

                    let cp1x = x1 + (x2 - x0) / 6;
                    let cp1y = y1 + (y2 - y0) / 6;

                    let cp2x = x2 - (x3 - x1) / 6;
                    let cp2y = y2 - (y3 - y1) / 6;

                    path += ` ${cp1x} ${cp1y} ${cp2x} ${cp2y} ${x2} ${y2}`;
                }

                return path;
            }

            const cardinalDots = (data) => {

                let size = data.length;
                let dotCoords = [];

                for (let i = 0; i < size; i += 2) {

                    let x = i ? data[i - 2] : data[0];
                    let y = i ? data[i - 1] : data[1];

                    dotCoords.push({
                        x: x,
                        y: y
                    })

                }

                return dotCoords;
            }

            const curTweenLine = TweenMax.to(pointsLine, 1, {
                endArray: endPoitsLine,
                repeat: 0,
                repeatDelay: 0,
                yoyo: false,
                ease: Sine.easeInOut,
                onUpdate: () => {
                    pointsCacheLine = pointsLine;
                    setSplineCoords(cardinalSpline(pointsLine, tension));
                },
                onComplete: () => {

                    if (isAnimatingLoop) {
                        animate(endPoitsLine)
                    }

                }
            }).duration(2);

            const curTweenDots = TweenMax.to(pointsDots, 1, {
                endArray: endPoitsDots,
                repeat: 0,
                repeatDelay: 0,
                yoyo: false,
                ease: Sine.easeInOut,
                onUpdate: () => {
                    pointsCacheDots = pointsDots;
                    setDotsCoords(cardinalDots(pointsDots));
                },
            }).duration(2);

            tweenLine = curTweenLine;
            tweenDots = curTweenDots;

        }

        const animateTransition = (points) => {

            sineCoords = getCoords(points).sine

            const cardinalSpline = (data, k) => {

                if (k == null) k = 1;

                let size = data.length;
                let last = size - 4;

                let path = `M ${data[0]} ${data[1]} C`;

                for (let i = 0; i < size - 2; i += 2) {

                    let x0 = i ? data[i - 2] : data[0];
                    let y0 = i ? data[i - 1] : data[1];

                    let x1 = data[i + 0];
                    let y1 = data[i + 1];

                    let x2 = data[i + 2];
                    let y2 = data[i + 3];

                    let x3 = i !== last ? data[i + 4] : x2;
                    let y3 = i !== last ? data[i + 5] : y2;

                    let cp1x = x1 + (x2 - x0) / 6 * k;
                    let cp1y = y1 + (y2 - y0) / 6 * k;

                    let cp2x = x2 - (x3 - x1) / 6 * k;
                    let cp2y = y2 - (y3 - y1) / 6 * k;

                    path += ` ${cp1x} ${cp1y} ${cp2x} ${cp2y} ${x2} ${y2}`;
                }

                return path;
            }

            const cardinalDots = (data) => {

                let size = data.length;
                let dotCoords = [];

                for (let i = 0; i < size; i += 2) {

                    let x = i ? data[i - 2] : data[0];
                    let y = i ? data[i - 1] : data[1];

                    dotCoords.push({
                        x: x,
                        y: y
                    })

                }

                return dotCoords;
            }

            tweenLineTransition = TweenMax.to(points, 1, {
                endArray: sineCoords,
                repeat: 0,
                repeatDelay: 0,
                yoyo: false,
                ease: Sine.easeInOut,
                onUpdate: () => {
                    setSplineCoords(cardinalSpline(points, 1));
                },
                onComplete: () => {
                    // if (isAnimatingLoop) {
                    //     animate(endPoits)
                    // }
                }
            }).duration(1);

            let pointsReverse = [];

            points.forEach((point, key) => {

                if (key % 2) {
                    pointsReverse.push(point * -1)
                } else {
                    pointsReverse.push(point)
                }
            });

            let sinePointsReverse = [];

            sineCoords.forEach((point, key) => {

                if (key % 2) {
                    sinePointsReverse.push(point * -1)
                } else {
                    sinePointsReverse.push(point)
                }
            });

            tweenDotsTransition = TweenMax.to(pointsReverse, 1, {
                endArray: sineCoords,
                repeat: 0,
                repeatDelay: 0,
                yoyo: false,
                ease: Sine.easeInOut,
                onUpdate: () => {
                    setDotsCoords(cardinalDots(pointsReverse));
                    // pointsCacheDots = pointsReverse
                },
                onComplete: () => {
                    moveDotsOnSine(sineCoords);
                }
            }).duration(1);

        }

        // AFTER TRANSITION MOVE DOTS ON SINE WAVE
        const moveDotsOnSine = (points) => {

            let widthBetweenPoints = points[2] - points[0];
            const offset = -widthBetweenPoints / 2;


            const cardinalDots = (data, percent) => {
                try {
                    const height = document.documentElement.clientHeight;
                    let size = data.length;
                    let dotCoords = [];

                    let amount = getDots().amount;

                    let count = 0;

                    // if(graph === null || graph.current === null || splineCoords == '') return dotCoords;

                    const path = graph.current;

                    const totalLength = path.getTotalLength();
                    const partLength = totalLength / (amount - 1);

                    let y = findYs(data[4] + (percent * widthBetweenPoints) + offset);

                    for (let i = 0; i < size; i += 2) {

                        let x = i ? data[i - 2] : data[0];
                        const coords = graph.current.getPointAtLength((partLength * (i / 2)) + (percent * partLength));

                        x = coords.x;
                        y = coords.y;

                        count = count + 1;

                        dotCoords.push({
                            x: x,
                            y: y
                        })

                    }
                    return dotCoords;
                } catch (e) {
                }
                return [];
            }

            let endPoints = [];

            points.forEach((point, key) => {

                if (key % 2) {
                    endPoints.push(point * -1) // Y-Achsis
                } else {
                    endPoints.push(point + widthBetweenPoints) // X-Achsis
                }

            });

            tweenDotsSine = TweenMax.to(points, 1, {
                endArray: endPoints,
                repeat: -1,
                repeatDelay: 0,
                yoyo: false,
                ease: Linear.easeNone,
                onUpdate: () => {
                    let progress = 1 - (endPoints[0] - points[0]) / widthBetweenPoints;
                    setDotsCoords(cardinalDots(points, progress));
                    pointsCacheDots = points
                }
            }).duration(4);

        }

        const onScroll = e => {

            let scrollPosition = e.target.documentElement.scrollTop;


            if (scrollPosition > 220 && tweenLine !== null) {

                tweenLine.kill();
                tweenDots.kill();
                tweenLine = null;
                tweenDots = null;

                animateTransition(pointsCacheLine);

            }

            if (scrollPosition < 200) {


                if (tweenDotsSine !== null) {
                    tweenDotsSine.kill();
                    tweenDotsSine = null;
                }

                if (tweenLineTransition !== null) {

                    tweenLineTransition.kill();
                    tweenDotsTransition.kill();

                    tweenLineTransition = null;
                    tweenDotsTransition = null;

                }

                if (tweenLine === null) {
                    animate(pointsCacheLine, pointsCacheDots);
                }

            }
        };

        window.addEventListener("scroll", onScroll);

        if (animationStarted === false) {
            animationStarted = true;
            animate();
        }
        return () => {
            window.removeEventListener("scroll", onScroll);
        }
    }, []);


    const findYs = (x) => {

        const path = graph.current;

        if (graph === null || graph.current === null || splineCoords == '') return 0;

        var pathLength = path.getTotalLength()
        var start = 0
        var end = pathLength
        var target = (start + end)

        // Ensure that x is within the range of the path
        x = Math.max(x, path.getPointAtLength(0).x)
        x = Math.min(x, path.getPointAtLength(pathLength).x)

        // Walk along the path using binary search
        // to locate the point with the supplied x value
        while (target >= start && target <= pathLength) {
            var pos = path.getPointAtLength(target)

            // use a threshold instead of strict equality
            // to handle javascript floating point precision
            if (Math.abs(pos.x - x) < 0.001) {
                return pos.y
            } else if (pos.x > x) {
                end = target
            } else {
                start = target
            }
            target = (start + end) / 2
        }
    }

    return (
        <div>
            {svg()}
        </div>
    );

};

export default SvgComponent;