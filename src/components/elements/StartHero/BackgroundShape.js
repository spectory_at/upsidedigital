import React, { useEffect, useRef, useState } from "react";

const BackgroundShape = ({ wrapper, className }) => {

    const [coordinates, setCoordinates] = useState('');
    const [offset, setOffset] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);
    const [windowHeight, setWindowHeight] = useState(50);

    const onResize = () => {

        let right = 0;
        let middle = 0;
        let third = 0;
        let leftY = 0;

        let height = document.documentElement.clientHeight;

        if(height < 700) height = 700;

        setWindowHeight(height + 50);
        setWindowWidth(document.documentElement.clientWidth);

        let bottom = (height);

        if (document.documentElement.clientWidth > 1100) {
            middle = (document.documentElement.clientWidth - 1100) / 2;
            right = middle + (1100 / 10 * 8);
            third = (right - middle) / 3 * 2;
            leftY = bottom + 30;

            if (document.documentElement.clientWidth > 1300) {
                leftY = bottom + 15;
                third = (right - middle) / 3 * 2.3;
                right = middle + (1100 / 10 * 7);
            }

            if (document.documentElement.clientWidth > 1700) {
                leftY = bottom - 40;
                third = (right - middle / 1.6);
                right = middle + (1100 / 10 * 8);

                // if (document.documentElement.clientWidth > 2000) {
                //     third = (right - (middle / 2))
                // }

            }

            if (document.documentElement.clientWidth > 2000) {
                leftY = bottom - 40;
                right = middle + (1100 / 10 * 8);
                middle = middle / 3 * 2;
                third = (right - middle);
                // if (document.documentElement.clientWidth > 2000) {
                //     third = (right - (middle / 2))
                // }


            }

            setOffset(0);
        } else {
            right = (document.documentElement.clientWidth / 10 * 7) * 2;
            middle = right / 2;
            third = right / 2 * 1.5;
            leftY = bottom + 40;

            setOffset(right / 2 - 20)
        }

        // setCoordinates(`M0-0V${bottom}s${cx2},${cy2},${ex},${ey}S${secondCx2}-${secondCy2},${secondEx}-${secondEy}`);


        setCoordinates(`M0-0V${leftY}Q${middle / 2},${bottom + 32},${middle},${bottom + 32}S${middle + ((third - middle) / 2)},${bottom + 32},${third},${bottom - 50}S${right},${(bottom + 32) / 2},${right},${0}`);

    }

    useEffect(() => {

        onResize();

        window.addEventListener("resize", onResize);

        return () => {
            window.removeEventListener("resize", onResize);
        }

    }, [])

    return (
        <>
            <svg className={className} xmlns="http://www.w3.org/2000/svg" style={{ position: 'absolute' }} width={windowWidth} height={windowHeight} viewBox={`0 0 ${windowWidth} ${windowHeight}`}>

                <defs>
                    <linearGradient id="start-hero-gradient" x1="0.5" x2="0.64" y2="0.932" gradientUnits="objectBoundingBox">
                        <stop offset="0" stop-color="#fe5000" stop-opacity="0.902" />
                        <stop offset="0.576" stop-color="#f65020" stop-opacity="0.902" />
                        <stop offset="1" stop-color="#FB8F22" stop-opacity="0.796" />
                    </linearGradient>
                    <filter id="Path_1" x="0" y="0" width={windowWidth} height={windowHeight} filterUnits="userSpaceOnUse">
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood flood-opacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                    </filter>
                </defs>
                <g transform="matrix(1, 0, 0, 1, -9, -6)" filter="url(#Path_1)">
                    <path id="Pfad_6" data-name="Pfad 606" transform={`translate(-${offset} 0)`} d={coordinates} fill="url(#start-hero-gradient)" />
                </g>
            </svg>

            {/* <div style={{ width: '100vw', position: 'fixed', top: 0, left: 0, height: '100vh', display: 'inline-block', textAlign: 'center' }}>
                <div style={{ width: 1100, height: 1000, display: 'inline-block', border: '1px solid rgba(0,0,0,0.1)' }}></div>
            </div> */}

        </>
    );

};

export default BackgroundShape;
