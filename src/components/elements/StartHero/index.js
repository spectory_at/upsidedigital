import React, { useEffect, useRef, useState } from "react";
import * as styles from "./starthero.module.scss";
import parse from "html-react-parser";
import Button from "../Button";
import SvgComponent from "./svg";
import BackgroundShape from "./BackgroundShape";
import StartHeroMobileShape from "./StartHeroMobileShape";
import NewShape from "../Hero/newShape";
import ScrollDownButton from "../ScrollDownButton";

const StartHeader = ({
    headline_1,
    headline_2,
    sub_headline_1,
    sub_headline_2,
    top_headline_1,
    top_headline_2,
    button_link,
    button_text,
}) => {
    const topHeadline = useRef();
    const headline = useRef();
    const subHeadline = useRef();
    const wrapper = useRef(null);

    const [fixed, setFixed] = useState(true);
    const [showSecondText, setShowSecondText] = useState(false);
    const [animating, setAnimating] = useState(false);
    const [marginBottom, setMarginBottom] = useState(370);

    let breakpointFixed = 370;

    useEffect(() => {

        let _showSecondText = false;

        const eventListener = () => {
            let scrollPosition = document.documentElement.scrollTop;

            let breakpointShowText = 220;
            if (document.documentElement.clientWidth < 1100) {
                breakpointFixed = 420;
                breakpointShowText = 220;
            }

            if (scrollPosition > breakpointFixed) {

                setFixed(false)

                if (document.documentElement.clientHeight > 1096) {
                    setMarginBottom(document.documentElement.clientHeight - 1096);
                }else{
                    setMarginBottom(0);
                }

            } else {
                
                setFixed(true);

                if (document.documentElement.clientHeight > 1096) {
                    setMarginBottom(document.documentElement.clientHeight - 1096 + 370);
                } else {
                    setMarginBottom(370);
                }
            }

            if (scrollPosition > breakpointShowText && !_showSecondText) {

                setAnimating(true);
                setTimeout(() => {
                    _showSecondText = true;
                    setShowSecondText(true);
                    setAnimating(false);
                }, 500)


            } else if (scrollPosition <= breakpointShowText && _showSecondText) {

                setAnimating(true);

                setTimeout(() => {
                    _showSecondText = false;
                    setShowSecondText(false);
                    setAnimating(false);
                }, 500);

            }
        };


        eventListener();

        window.addEventListener("scroll", eventListener);
        return () => {
            window.removeEventListener("scroll", eventListener);
        }
    }, []);


    const setContainerHeight = (height) => {

        // wrapper.current.style.height = height + 'px';

        if (wrapper.current.getElementsByClassName('container') !== undefined) {
            // alert("wrapper" + JSON.stringify(wrapper))
            wrapper.current.getElementsByClassName('container')[0].style.height = wrapper.current.clientHeight + 50 + 'px';
        }
    }

    return (
        <>
            <div className={`${styles.heroWrapperOutter} ${fixed && styles.mt}`} ref={wrapper}
                style={{
                    marginBottom: marginBottom
                }}
            >
                <div className={`${styles.animationContainer} ${fixed && styles.mt}`}>
                    <SvgComponent wrapper={wrapper} />
                </div>
                <StartHeroMobileShape className={styles.heroShapeMobile} wrapper={wrapper} setParentHeight={setContainerHeight} />
                <div
                    // ref={wrapper}
                    className={`${styles.heroWrapper} ${fixed && styles.mt}`}
                >
                    <div className={`${styles.heroContainer} ${fixed && styles.fixed}`}>
                        <div style={{ position: "relative", "z-index": "3" }}>
                            {/* <div className={styles.heroShapeMobile}></div> */}
                            <NewShape className={styles.heroShape} isStart={true} wrapper={wrapper} />
                            <div className={`container ${styles.contentContainer} ${animating ? styles.hide : styles.show} `}>
                                <div className="col8">
                                    {!showSecondText &&
                                        <div
                                            id="firstTextContainer"
                                            className={`${styles.textContainer}`}
                                        >
                                            <div>
                                                <h5 ref={topHeadline}>{parse(top_headline_1)}</h5>
                                                <h1 ref={headline}>{parse(headline_1)}</h1>
                                                <div className={`${styles.subheadline} ${styles.first}`} ref={subHeadline}>{parse(sub_headline_1)}</div>
                                                <Button
                                                    destination={button_link}
                                                    title={button_text}
                                                    withArrow={true}
                                                    type={"secondary"}
                                                />
                                                <ScrollDownButton
                                                    label={button_text}
                                                    destination={button_link}
                                                />
                                            </div>
                                        </div>
                                    }
                                    {showSecondText &&
                                        <div
                                            id="secondTextContainer"
                                            className={`${styles.textContainer}`}
                                        >
                                            <div>
                                                <h5>{parse(top_headline_2)}</h5>
                                                <h1>{parse(headline_2)}</h1>
                                                <div className={styles.subheadline}>{parse(sub_headline_2)}</div>
                                                <div style={{ opacity: 0, marginTop: 20, pointerEvents: 'none' }}>
                                                    <Button
                                                        destination={button_link}
                                                        title={button_text}
                                                        type={"secondary"}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*{!showSecondText && <div className={styles.press}></div>}*/}
        </>
    );
};

export default StartHeader;
