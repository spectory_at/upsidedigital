import React, { useEffect, useRef, useState } from "react";

const BackgroundShapeMobile = ({ wrapper, className, style, setParentHeight, normal, big, type, initHeight }) => {


    const [coordinates, setCoordinates] = useState('');
    const [offset, setOffset] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);
    const [windowHeight, setWindowHeight] = useState(0);
    // const [initHeight, setInitHeight] = useState(null);
    const [reloading, setReloading] = useState(false);

    const onResize = () => {

        // setWindowWidth(wrapper.current.clientWidth);
        // setCoordinates(`M0 0H${width}V${bottom}S${(width / 4 * 3)},${bottom - 50},${width / 2},${bottom}S${(width / 7)},${bottom + 30},${0},${bottom - 50}Z`);

        if (setParentHeight !== undefined && wrapper.current !== undefined) {

            let width = document.documentElement.clientWidth;
            if (width == windowWidth) {
                return;
            }

            let bottom = document.documentElement.clientHeight - 80;
            let offsetRight = 100;
            let offsetCenter = 0;

            if (width < 768) {
                bottom = document.documentElement.clientHeight - 60;
                offsetRight = 80;
                offsetCenter = 20;
            }

            let height = document.documentElement.clientHeight;


            if ((document.documentElement.clientHeight < wrapper.current.clientHeight && wrapper.current.clientHeight > 900) || type === 'fourth' || (big !== undefined && big !== false)) {

                let _initHeight = initHeight

                if (initHeight === null) {
                    // setInitHeight(wrapper.current.clientHeight)
                    _initHeight = wrapper.current.clientHeight;
                }

                if (big !== undefined && big !== false && type !== 'fourth') {

                    bottom = _initHeight + 60;
                    setWindowHeight(_initHeight + 100);

                    if (setParentHeight !== undefined) {
                        setParentHeight(_initHeight + 100);
                    }

                    if (width < 992) {
                        bottom = 1500 - 60;
                        setWindowHeight(1500);
                        if (setParentHeight !== undefined) {
                            setParentHeight(1500);
                        }
                    }
                    else if (width < 768) {
                        bottom = 1200 - 60;
                        setWindowHeight(1200);
                        if (setParentHeight !== undefined) {
                            setParentHeight(1200);
                        }
                    } else {
                        bottom = 900 - 60;
                        setWindowHeight(900);
                        if (setParentHeight !== undefined) {
                            setParentHeight(900);
                        }
                    }
                } else if (type === 'fourth') {

                    if (width < 340) {
                        bottom = 750 + 50;
                        setWindowHeight(750 + 80);

                        if (setParentHeight !== undefined) {
                            setParentHeight(750);
                        }
                    } else if (width < 768) {
                        bottom = _initHeight - 50;
                        setWindowHeight(_initHeight - 20);

                        if (setParentHeight !== undefined) {
                            setParentHeight(_initHeight);
                        }
                    } else if (width < 992) {
                        bottom = 900 - 50;
                        setWindowHeight(900 - 20);
                        if (setParentHeight !== undefined) {
                            setParentHeight(900);
                        }
                    } else {
                        bottom = _initHeight + 150;
                        setWindowHeight(_initHeight + 180);

                        if (setParentHeight !== undefined) {
                            setParentHeight(_initHeight + 200);
                        }
                    }


                }

            } else {

                if (normal !== undefined && width < 768) {

                    setWindowHeight(height + 100);
                    bottom = height;
                    if (setParentHeight !== undefined) {
                        // setParentHeight(height - 200);
                    }

                } else {


                    setWindowHeight(height + 50);
                    if (setParentHeight !== undefined) {
                        setParentHeight(height + 50);
                    }
                }

            }

            setWindowWidth(wrapper.current.clientWidth);
            setCoordinates(`M0 0V${bottom}S${(width / 4 * 3)},${bottom + offsetCenter},${width},${bottom - offsetRight}V0Z`);

        }

    }

    const reload = () => {
        setReloading(true)
        setTimeout(() => {
            setReloading(false)
        }, 10)
    }

    useEffect(() => {
        onResize();
    }, [initHeight])

    useEffect(() => {

        onResize();
        reload();

        window.addEventListener("resize", () => onResize());


        return () => {
            window.removeEventListener("resize", onResize);
        }

    }, [])


    if (reloading) {
        return null;
    }


    return (
        <>
            <svg className={className} width={windowWidth} height={windowHeight} viewBox={`0 0 ${windowWidth} ${windowHeight}`}>

                <defs>
                    <linearGradient id="start-hero-mobile-gradient" x1="0.5" x2="0.64" y2="0.932" gradientUnits="objectBoundingBox">
                        <stop offset="0" stopColor="#fe5000" stopOpacity="0.902" />
                        <stop offset="0.576" stopColor="#f65020" stopOpacity="0.902" />
                        <stop offset="1" stopColor="#FB8F22" stopOpacity="0.796" />
                    </linearGradient>
                    <filter id="start-hero-mobile-shadow" x="0" y="0" width={windowWidth} height={windowHeight} filterUnits="userSpaceOnUse">
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood floodOpacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                    </filter>
                </defs>
                <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#start-hero-mobile-shadow)">
                    <path id="start-hero-mobile" data-name="Start Hero Mobile" d={coordinates} fill="url(#start-hero-mobile-gradient)" />
                </g>
            </svg>

        </>
    );

};

export default BackgroundShapeMobile;