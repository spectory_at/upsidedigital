import React, { useEffect, useRef, useState } from "react"
import * as styles from "./styles.module.scss"
import elementInViewport from "../../../func/elementInViewPort";

const Video = ({ url, style }) => {

    const [responsiveStyles, setResponsiveStyles] = useState({});
    const [containerHeight, setContainerHeight] = useState(style.width);

    const ref = useRef(null)
    let played = false;
    useEffect(() => {
        if (ref.current) {
            document.addEventListener("scroll", () => {

                onScroll();
                
                if (!played) {
                    if (elementInViewport(ref.current)) {
                        ref.current.play();
                        played = true;
                    }
                }
            })
        }
        ref.current.onended = () => {
            played = true;
            // replayRef.current.classList.add(styles.active);
        }

        return () => {
            if (ref.current) {
                document.removeEventListener("scroll", () => { })
            }
        }

    }, [])


    const calcStyles = (e) => {
        const windowWidth = document.documentElement.clientWidth;

        let sProps = {
            maxWidth: style.width,
            display: "inline-block"
        }

        if (windowWidth <= 1100) {
            sProps.maxWidth = style.width
        }

        if (windowWidth <= 992) {

            if (style.widthTablet !== null) {
                sProps.maxWidth = style.widthTablet;
            }
        }

        if (windowWidth <= 768) {
            if (style.widthMobile !== null) {
                sProps.maxWidth = style.widthMobile;
            }
        }

        if (style.position == "center") {
            sProps.marginLeft = "auto";
            sProps.marginRight = "auto";
        } else if (style.position == "right") {
            sProps.float = "right";
        }

        setResponsiveStyles(sProps);

        // SET HEIGHT: 




    }

    const onScroll = () => {
        if (ref.current != null) {
            setContainerHeight(ref.current.clientHeight);
        }
    }

    useEffect(() => {

        calcStyles();
        window.addEventListener('resize', calcStyles);

        return () => {
            window.addEventListener('resize', calcStyles);
        }
    }, [])


    return <div className={styles.video}>
        <div className={styles.videoBorderHideWrapper} style={{ ...responsiveStyles, height: containerHeight }}>
            <video muted={true} src={url} ref={ref} type='video/mp4' playsInline={true} disablePictureInPicture={true}></video>
        </div>
        {/*<div ref={replayRef} className={styles.replayButton}>*/}
        {/*    <IoReloadCircleOutline onClick={() => {*/}
        {/*        ref.current.play();*/}
        {/*        played = false;*/}
        {/*        replayRef.current.classList.remove(styles.active)*/}
        {/*    }}/>*/}
        {/*</div>*/}
    </div>
}

export default Video
