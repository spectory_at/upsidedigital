import React, { useEffect, useRef, useState } from "react"
import * as styles from "./styles.module.scss"

const HoverIcon = ({ icon, style }) => {
    
    const ref = useRef();
    const containerRef = useRef();

    useEffect(() => {
        if (ref.current) {
            containerRef.current.addEventListener("mouseenter", () => {
                ref.current.play();
            })
            containerRef.current.addEventListener("mouseleave", () => {
            ref.current.pause();
            })
        }

        return () => {
            document.removeEventListener("mouseenter", () => { })
            document.removeEventListener("mouseleave", () =>{})
        }
    }, [])

    return <div className={styles.video} ref={containerRef}>
        <video muted={true} src={icon} ref={ref} type='video/mp4' playsInline={true} disablePictureInPicture={true}></video>
    </div>
}

export default HoverIcon
