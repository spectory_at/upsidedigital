import React, { useState, useEffect } from "react";
import { Parallax, ParallaxProvider } from "react-scroll-parallax";

import * as styles from "./dynamicimages.module.scss";

const DynamicImages = (props) => {
    const [displayParallax, setDeisplayParallax] = useState(true);

    const checkWindowSize = () => {
        return document.documentElement.clientWidth;
    };

    const parallaxSwitcher = () => {
        if (checkWindowSize() < 1100 && !displayParallax) {
            setDeisplayParallax(true);
        } else if (checkWindowSize() > 1100 && displayParallax) {
            setDeisplayParallax(false);
        }
    };

    useEffect(() => {
        parallaxSwitcher();

        const listener = () => {
            parallaxSwitcher();
        };

        window.addEventListener("resize", listener);
        return () => {
            window.removeEventListener("resize", listener);
        };

    }, []);


    return (
        <ParallaxProvider>
            <div className={`${styles.container} ${(props.image_third !== null && props.image_third !== '') ? styles.three : styles.two} `}>
                <Parallax
                    className={`${styles.imageContainer} ${styles.first
                        } `}
                    y={[-60, 20]}
                    tagOuter="figure"
                    disabled={displayParallax}
                >
                    <div>
                        <img src={props.image_first} />
                    </div>
                </Parallax>
                <Parallax
                    className={`${styles.imageContainer} ${styles.second
                        } `}
                    y={[-40, 20]}
                    tagOuter="figure"
                    disabled={displayParallax}
                >
                    <div>
                        <img src={props.image_second} />
                    </div>
                </Parallax>
                {
                    props.image_third !== null && props.image_third !== '' &&
                    <Parallax
                        className={`${styles.imageContainer} ${styles.third
                            } `}
                        y={[-40, 20]}
                        tagOuter="figure"
                        disabled={displayParallax}
                    >
                        <div>
                            <img src={props.image_third} />
                        </div>
                    </Parallax>
                }

            </div>
        </ParallaxProvider>
    );
};

export default DynamicImages;
