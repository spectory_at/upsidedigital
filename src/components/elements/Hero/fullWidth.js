import React, { useEffect, useRef, useState } from "react";

const FullWidthShape = ({ wrapper, className }) => {

    const [coordinates, setCoordinates] = useState('');
    const [offset, setOffset] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);
    const [windowHeight, setWindowHeight] = useState(50);

    const onResize = () => {
        let width = document.documentElement.clientWidth;

        let height = document.documentElement.clientHeight;

        const maxHeight = 1096;
        const minHeight = 600;

        if (height > maxHeight) height = maxHeight;
        if (height < minHeight) height = minHeight;

        setWindowHeight(height + 50);

        let bottom = height;


        setCoordinates(
            `M0 0H${width}V${bottom - 150}S${(width / 4) * 3.4},${bottom + 50},${width / 1.8},${bottom} S${width / 7},${bottom - 45},${0},${bottom - 20}Z`,
        );

        setWindowWidth(document.documentElement.clientWidth);



        // setCoordinates(
        // 	`M0,0H${width}V${bottom}.4s-259-91.794-538-67.482C970.59,872.834,817.432,902.445,538,904.4,248.767,906.423,0,832.918,0,832.918Z`,
        // );
    };

    useEffect(() => {
        onResize();
        window.addEventListener("resize", onResize);

        return () => {
            window.removeEventListener("resize", onResize);
        };
    }, []);

    // const width = right + 50 * 100; //366.478;

    return (
        <>
            <svg
                className={className}
                xmlns="http://www.w3.org/2000/svg"
                width={windowWidth}
                height={windowHeight}
                viewBox={`0 0 ${windowWidth} ${windowHeight}`}
            >
                <defs>
                    <linearGradient
                        id="fw-shape-gradient"
                        x1="0.444"
                        y1="0.77"
                        x2="0.445"
                        y2="0.17"
                        gradientUnits="objectBoundingBox"
                    >
                        <stop offset="0" stop-color="#fb8f22" />
                        <stop offset="0.517" stop-color="#f65020" stop-opacity="0.902" />
                        <stop offset="1" stop-color="#fe5000" />
                    </linearGradient>

                    <filter
                        id="Path_FW_Shape"
                        x="0"
                        y="0"
                        width={windowWidth}
                        height={windowHeight}
                        filterUnits="userSpaceOnUse"
                    >
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood flood-opacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                    </filter>
                </defs>
                <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#Path_FW_Shape)">
                    <path
                        id="Pfad_FW_SHAPE_1"
                        data-name="Pfad 604"
                        d={coordinates}
                        fill="url(#fw-shape-gradient)"
                    />
                </g>
            </svg>

            {/* <div style={{ width: '100vw', position: 'fixed', top: 0, left: 0, height: '100vh', display: 'inline-block', textAlign: 'center' }}>
                <div style={{ width: 1100, height: 1000, display: 'inline-block', border: '1px solid rgba(0,0,0,0.1)' }}></div>
            </div> */}
        </>
    );

};

export default FullWidthShape;
