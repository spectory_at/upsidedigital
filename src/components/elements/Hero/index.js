import React, { useEffect, useRef, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";

import * as styles from "./styles.module.scss";

import getPosition from "../../../func/elementPosition";
import LineAnimation from "./lineAnimation";
import renderElements from "..";
import windowSize from "../../../func/windowSize";
import BackgroundShape from "../StartHero/BackgroundShape";
import BackgroundShapeMobile from "../StartHero/BackgroundShapeMobile";
import BackgroundShapeFullWidth from "./fullWidth";
import NewShape from "./newShape";
import FullWidthShape from "./fullWidth";

const Hero = (props) => {
    const wrapper = useRef(null);
    const [showAnimation, setShowAnimation] = useState(false);
    const [largerThanScreen, setLargerThanScreen] = useState(false);
    const [initHeight, setInitHeight] = useState(null);

    const isDesktop = () => {
        if (windowSize() > 1100) {
            return true;
        }
        return false;
    };

    useEffect(() => {

        setShowAnimation(isDesktop());
        setTimeout(() => {
            if (wrapper.current !== null) {
                setInitHeight(wrapper.current.clientHeight);
            }
        }, 200)

        const listener = (e) => {
            setShowAnimation(isDesktop());
        };

        window.addEventListener("resize", listener);
        return () => {
            window.removeEventListener("resize", listener);
        };


    }, []);


    const setContainerHeight = (height) => {
        wrapper.current.style.height = height + "px";

        if (wrapper.current.getElementsByClassName("container") !== undefined) {
            // alert("wrapper" + JSON.stringify(wrapper))
            wrapper.current.getElementsByClassName("container")[0].style.height =
                wrapper.current.clientHeight + "px";
        }

        if (
            wrapper.current.clientHeight > document.documentElement.clientHeight
        ) {
            setLargerThanScreen(true);
        }



    };

    console.log(props.children);

    // Add Arrow to buttons
    props.children[0].children[0].children.forEach((child, key) => {
        if (child.id === 'button') {
            props.children[0].children[0].children[key].withArrow = true;
        }
    })

    return (
        <>
            <div
                ref={wrapper}
                className={`${styles.hero} ${styles[props.type]} ${largerThanScreen && "largerThanScreen"
                    }`}
            >

                {props.type === 'third' ? <><FullWidthShape
                    wrapper={wrapper}
                    className={styles.shape}
                />
                    <BackgroundShapeMobile
                        setParentHeight={setContainerHeight}
                        wrapper={wrapper}
                        className={styles.mobileShape}
                        normal
                        big={true}
                        initHeight={initHeight}
                    />
                </> :
                    <><NewShape
                        wrapper={wrapper}
                        className={styles.shape}
                    />
                        <BackgroundShapeMobile
                            setParentHeight={setContainerHeight}
                            wrapper={wrapper}
                            className={styles.mobileShape}
                            normal
                            type={props.type}
                            big={props.type === 'fourth'}
                            initHeight={initHeight}
                        />
                    </>
                }

                <div className={styles.wrapper}>
                    {showAnimation && (
                        <LineAnimation />
                    )}
                    {renderElements(props.children, props.lang)}
                </div>
            </div>
        </>
    );
};

// function mapStateToProps(state) {
//     return {
//         animationPointsPositionOnPage:
//             state.app.animationPointsPositionOnPage || undefined,
//     };
// }

// export default connect(mapStateToProps, null)(Hero);

export default Hero;
