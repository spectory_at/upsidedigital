import React, { useEffect, useRef, useState } from "react";

const NewShape = ({ wrapper, className, isStart }) => {

    const [coordinates, setCoordinates] = useState('');
    const [offset, setOffset] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);
    const [windowHeight, setWindowHeight] = useState(50);
    const [marginBottom, setMarginBottom] = useState(null);

    const onResize = () => {

        let right = 0;
        let middle = 0;
        let third = 0;
        let leftY = 0;

        let height = document.documentElement.clientHeight;

        const maxHeight = 1070;

        if (height > maxHeight) height = maxHeight;
        if (height < 600) height = 600;

        setWindowHeight(height + 50);
        setWindowWidth(document.documentElement.clientWidth);

        let bottom = (height + 30);
        middle = (document.documentElement.clientWidth - 1320) / 2;
        right = middle + ((1320 / 10) * 7.45);
        leftY = bottom;


        // if (document.documentElement.clientWidth > 1320) {

        //     third = (right - middle) / 3 * 2;
        //     leftY = bottom + 30;

        //     setOffset(0);
        // } else {
        //     right = (document.documentElement.clientWidth / 10 * 7) * 2;
        //     middle = right / 2;
        //     third = right / 2 * 1.5;
        //     leftY = bottom + 40;

        //     setOffset(right / 2 - 20)
        // }

        // setCoordinates(`M0-0V${bottom}s${cx2},${cy2},${ex},${ey}S${secondCx2}-${secondCy2},${secondEx}-${secondEy}`);

        // Q${middle / 2},${bottom + 32},${middle},${bottom + 32}S${middle + ((third - middle) / 2)},${bottom + 32},${third},${bottom - 50}

        setCoordinates(`M${right},0s${right / 100},${bottom}-${right - middle},${bottom}H0V0Z`);

    }

    useEffect(() => {

        onResize();

        window.addEventListener("resize", onResize);

        return () => {
            window.removeEventListener("resize", onResize);
        }

    }, [])

    return (
        <>
            <svg className={className} xmlns="http://www.w3.org/2000/svg" style={{ position: 'absolute' }} width={windowWidth} height={windowHeight} viewBox={`0 0 ${windowWidth} ${windowHeight}`}>

                <defs>
                    <linearGradient id="start-hero-gradient" x1="0.5" x2="0.64" y2="0.932" gradientUnits="objectBoundingBox">
                        <stop offset="0" stopColor="#fe5000" stopOpacity="0.902" />
                        <stop offset="0.576" stopColor="#f65020" stopOpacity="0.902" />
                        <stop offset="1" stopColor="#FB8F22" stopOpacity="0.796" />
                    </linearGradient>
                    <filter id="Path_1" x="0" y="0" width={windowWidth} height={windowHeight} filterUnits="userSpaceOnUse">
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood floodOpacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                    </filter>
                </defs>
                <g transform="matrix(1, 0, 0, 1, -9, -6)" filter="url(#Path_1)">
                    <path id="Pfad_6" data-name="Pfad 606" transform={`translate(-${offset} 0)`} d={coordinates} fill="url(#start-hero-gradient)" />
                </g>
            </svg>

            {/* <div style={{ width: '100vw', position: 'fixed', top: 0, left: 0, height: '100vh', display: 'inline-block', textAlign: 'center' }}>
                <div style={{ width: 1320, height: 1000, display: 'inline-block', border: '1px solid rgba(0,0,0,0.1)' }}></div>
            </div> */}
        </>
    );

};

export default NewShape;
