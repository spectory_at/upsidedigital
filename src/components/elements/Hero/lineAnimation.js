import React, { useContext, useRef, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import * as styles from "./lineAnimation.module.scss";
const LineAnimation = ({ }) => {
    const [haveImage, setHaveImage] = useState(false);

    const svgMaxHeight = 9637;
    const paddingTop = 50;
    const pointSize = 100;

    //Get dom elements object
    let pathRef = useRef();
    let wrapperRef = useRef();
    let pointRef = useRef();

    const points = useSelector(state => state.app.animationPointsPositionOnPage);



    useEffect(() => {
        if (points === undefined || points.length < 1) return;

        // Clone point svg object into varibale
        let point = pointRef.current.cloneNode(true);
        // Remove svg from dom.
        pointRef.current.remove();

        //Set dom elements for each point position.
        if (points.length > 0) {
            points.forEach((e) => {
                if (e.image) {
                    setHaveImage(true)
                    e.element = document.createElement("div");
                    e.element.classList.add(styles.pointImage);
                    e.element.innerHTML = "<img src=" + e.image + ">";

                } else {
                    e.element = point.cloneNode(true);
                }

                e.element.style.setProperty("top", e.position + "px");
                wrapperRef.current.appendChild(e.element);
            });
        }


        //EventListener function
        const eventListener = () => {
            if (
                window.scrollY + window.innerHeight / 2 <
                points[points.length - 1].position + paddingTop - pointSize / 2
            ) {
                pathRef.current.style.setProperty(
                    "stroke-dashoffset",
                    "" +
                    svgMaxHeight -
                    (window.scrollY +
                        (window.scrollY > paddingTop
                            ? window.innerHeight / 2 - paddingTop
                            : 0)),
                );
            }

            //Display point by position
            points.forEach((e) => {
                if (window.pageYOffset + window.innerHeight / 2 >= e.position) {
                    if (!e.element.classList.contains(styles.show)) {
                        e.element.classList.add(styles.show);
                    }
                } else {
                    if (e.element.classList.contains(styles.show)) {
                        e.element.classList.remove(styles.show);
                    }
                }
            });
        };

        //Add scroll event listener
        window.addEventListener("scroll", eventListener);
        return () => {
            window.removeEventListener("scroll", eventListener);
        };
    }, [points]);

    if (points === undefined || points.length < 1) return null;
    
    return (
        <div
            className="container"
            style={{
                alignItems: "flex-start",
                zIndex: "-1",
                position: "absolute",
                width: "100%",
                alignSelf: "center",
                transform: "translateX(-50%)",
                left: "50%",
            }}
        >
            <div className="col6"></div>
            <div className="col6" style={{ position: "relative" }}>
                <div
                    style={{ height: points[points.length - 1].position + (haveImage ? 150 : 100) + "px" }}
                    ref={wrapperRef}
                    className={`${styles.lineAnimationWrapper}`}
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="198.387"
                        height={points[points.length - 1].position}
                        viewBox={"0 0 198.387 " + points[points.length - 1].position}
                    >
                        <path
                            className={styles.path}
                            ref={pathRef}
                            id="Path_79"
                            data-name="Path 79"
                            d={
                                "M6543.593-4015.37c120.8.893,118.133,184.4,118.133,431.839,0,207.37,7.658,881.287,7.658,1053V" +
                                // "M6498.617-4019.54s51.574-23.249,101.035,53.118c39.881,61.575,78.389,188.218,87.135,445q1.787,52.459,1.83,113.127V" +
                                +points[points.length - 1].position
                            }
                            transform="translate(-6493.729 4026.388)"
                            fill="none"
                            stroke="#fe5000"
                            strokeLinecap="round"
                            strokeWidth="7"
                        />
                    </svg>

                    <div ref={pointRef} className={styles.point}>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="88"
                            height="88"
                            viewBox="0 0 88 88"
                        >
                            <defs>
                                <linearGradient
                                    id="linear-gradient"
                                    y1="0.5"
                                    x2="1.142"
                                    y2="0.5"
                                    gradientUnits="objectBoundingBox"
                                >
                                    <stop offset="0" stopColor="#fe5000" />
                                    <stop offset="0.26" stopColor="#ef4b20" />
                                    <stop offset="1" stopColor="#fb8f22" />
                                    {/* stop-opacity="0.729" */}
                                    <stop offset="1" stopColor="#bda79e" />
                                </linearGradient>
                                <filter
                                    id="Rectangle_67"
                                    x="0"
                                    y="0"
                                    width="88"
                                    height="88"
                                    filterUnits="userSpaceOnUse"
                                >
                                    <feOffset dy="3" input="SourceAlpha" />
                                    <feGaussianBlur stdDeviation="3" result="blur" />
                                    <feFlood floodOpacity="0.161" />
                                    <feComposite operator="in" in2="blur" />
                                    <feComposite in="SourceGraphic" />
                                </filter>
                            </defs>
                            <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#Rectangle_67)">
                                <rect
                                    id="Rectangle_67-2"
                                    data-name="Rectangle 67"
                                    width="70"
                                    height="70"
                                    rx="35"
                                    transform="translate(79 0) rotate(90)"
                                    fill="url(#linear-gradient)"
                                />
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default LineAnimation;
