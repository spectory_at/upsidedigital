/**
 * Page elements like Text, Headline, Slider, etc.
 */
import React from "react";
import { v4 as uuidv4 } from "uuid";
import Button from "./Button";
import CallToActionContainer from "./CallToAction/container";
import Career from "./Career";
import ContactCard from "./ContactCard";
import DynamicImages from "./DynamicImages";
import BigForm from "./Form/big";
import SmallForm from "./Form/small";
import Headline from "./Headline";
import Hero from "./Hero";
import Image from "./Image";
import ListItemsContainer from "./ListItems/Container";
import ListItem from "./ListItems/ListItem";
import Person from "./Person";
import Row from "./Row";
import Stage from "./Stage";
import StartHero from "./StartHero";
import TestimonialsContainer from "./Testimonials/container";
import TestimonialsItem from "./Testimonials/item";
import CloudCommentsContainer from "./CloudComments/container";
import CloudCommentsItem from "./CloudComments/item";
import Text from "./Text";
import Video from "./Video";
import JobOffers from "./JobsOffers";
import JobApplicationForm from "./Form/jobApplication";
import JobApplicationFormBig from "./Form/jobApplicationBig";
import HoverIcon from "./HoverIcon";
import DisplayScreen from "./DisplayScreen";
import AlignItem from "./AlignItem";
import Spacer from "./Spacer";
import ImageLogoContainer from "./ImageLogoContainer";
import BlogPosts from "./BlogPosts";

const renderElements = (content_raw, lang = "de") => {
    let content = content_raw;
    if (typeof content_raw !== "object") {
        try {
            content = JSON.parse(content_raw);
        } catch {
            return "NO OBJECT FOUND";
        }
    }

    if (content === null) {
        return "NO OBJECT FOUND";
    }
    return content.map((element) => {
        if (element != null) {

            element.lang = lang;

            switch (element.id) {
                case "row":
                    return <Row key={uuidv4()} {...element} />;
                case "hero":
                    return <Hero key={uuidv4()} {...element} />;
                case "text":
                    return <Text key={uuidv4()} {...element} />;
                case "headline":
                    return <Headline key={uuidv4()} {...element} />;
                case "button":
                    return <Button key={uuidv4()} {...element} />;
                case "single_image":
                    return <Image key={uuidv4()} {...element} />;
                case "single_video":
                    return <Video key={uuidv4()} {...element} />;
                case "list_container":
                    return <ListItemsContainer key={uuidv4()} {...element} />;
                case "list_item":
                    return <ListItem key={uuidv4()} {...element} />;
                case "small_contact_form":
                    return <SmallForm key={uuidv4()} {...element} />;
                case "contact_form":
                    return <BigForm key={uuidv4()} {...element} />;
                case "cloud_comments_container":
                    return <CloudCommentsContainer key={uuidv4()} {...element} />;
                case "cloud_comment":
                    return <CloudCommentsItem key={uuidv4()} {...element} />;
                case "call_to_action_container":
                    return <CallToActionContainer key={uuidv4()} {...element} />;
                case "start_header":
                    return <StartHero key={uuidv4()} {...element} />;
                case "contact_card":
                    return <ContactCard key={uuidv4()} {...element} />;
                case "career":
                    return <Career key={uuidv4()} {...element} />;
                case "person":
                    return <Person key={uuidv4()} {...element} />;
                case "dynamic_images":
                    return <DynamicImages {...element} />;
                case "stage":
                    return <Stage {...element} />;
                case "open_jobs":
                    return <JobOffers key={uuidv4()} {...element} />;
                case "application_form":

                    if (element.type !== undefined && element.type === 'big') {
                        return <JobApplicationFormBig key={uuidv4()} {...element} />;
                    }
                    return <JobApplicationForm key={uuidv4()} {...element} />;
                case "hover_icon":
                    return <HoverIcon key={uuidv4()} {...element} />;
                case "display_screen":
                    return <DisplayScreen key={uuidv4()} {...element} />;
                case "spacer":
                    return <Spacer key={uuidv4()} {...element} />;
                case "image_logo_container":
                    return <ImageLogoContainer key={uuidv4()} {...element} />;
                case "align_item":
                    return <AlignItem key={uuidv4()} {...element} />;
                case "blog_posts":
                    return <BlogPosts key={uuidv4()} {...element} />;
                default:
                    return <div key={uuidv4()}></div>;
            }
        }
    });
};

export default renderElements;
