import React, { useEffect, useRef } from "react"
import * as styles from "./button.module.scss"
import { Link } from "@reach/router"
import { withPrefix } from "gatsby-link"
import ArrowIcon from "../../../images/arrow.svg";
import elementInViewport from "../../../func/elementInViewPort";
import animateCSS from "../../../func/animateCSS";
import smoothscroll from 'smoothscroll-polyfill';

const Button = (props) => {

    const ref = useRef()

    useEffect(() => {

        let animatedLocal = false;
        smoothscroll.polyfill();
        const check = () => {
            if (ref.current && props.animate) {
                if (elementInViewport(ref.current)) {
                    if (!animatedLocal) {
                        animateCSS(ref.current, 'fadeInUp').then((message) => {
                            ref.current.classList.add(styles.show)
                            // setAnimated(true)
                        });
                        animatedLocal = true;
                    }
                }
            }
        }

        check(ref);

        document.addEventListener("scroll", () => {

            check(ref);

        })

    }, [])

    let marginStyle = {};

    if (typeof props.style != "undefined") {
        marginStyle.marginTop = props.style.verticalMargin;
        marginStyle.marginBottom = props.style.verticalMargin;
    }

    if (props.destination !== undefined) {

        if (props.destination.includes('http') || props.destination.includes('mailto') || props.destination.includes('tel')) {

            return <a
                className={`button ${styles.button} ${styles[props.type]} ${styles[props.buttonState]} ${!props.animate && styles.show}`}
                href={props.destination}
                ref={ref}
                target={props.target || ''}

                style={{ ...props.additionalStyle, ...marginStyle } || {}}
            >
                {props.title}
            </a>

        } else if (props.destination.charAt(0) === '#') {

            let scrollTo = 0;

            if (props.destination === '#second-animation') {
                scrollTo = 230;
            } else if (props.destination === '#down') {
                if (typeof window !== "undefined") {
                    scrollTo = window.innerHeight;
                }
            } else {

                if (typeof window !== "undefined") {
                    let destinationElement = document.getElementById(props.destination.replace('#', ''));

                    if (destinationElement !== null) {
                        if (props.destination === '#posts') {
                            scrollTo = window.innerWidth >= 992 ? destinationElement.offsetTop + 600 : window.innerHeight - 60;
                        } else {
                            scrollTo = destinationElement.offsetTop - 100;
                        }
                    }

                }

            }

            return <div
                ref={ref}
                className={`button ${styles.button} ${styles[props.type]} ${styles[props.buttonState]} ${!props.animate && styles.show} ${props.withArrow ? styles.withArrow : null}`}
                style={props.additionalStyle || {}}
                onClick={() => {

                    console.log(scrollTo);
                    window.scrollTo({
                        top: scrollTo,
                        left: 0,
                        behavior: 'smooth'
                    });

                }}
            >
                {props.title}
                {props.withArrow && <ArrowIcon className={styles.arrow} />}
            </div>

        } else {



        }

    } else if (props.onClick !== undefined) {

        return <div
            className={`button ${styles.button} ${styles[props.type]} ${styles[props.buttonState]} ${!props.animate && styles.show}`}
            onClick={props.onClick}
            ref={ref}
            style={{ ...props.additionalStyle, ...marginStyle } || {}}
        >
            {props.title}
        </div>

    }

    return (
        <Link
            ref={ref}
            className={`button ${styles.button} ${styles[props.type]} ${styles[props.buttonState]} ${!props.animate && styles.show}`}
            to={withPrefix(props.destination)}
            target={props.target || ''}
            style={{ ...props.additionalStyle, ...marginStyle } || {}}
        >
            {props.title}

        </Link>
    )

    if (props.destination !== undefined) {


    } else if (props.type === 'submit') {
        return (
            <input
                type="submit"
                value={props.title || "Absenden"}
                className={` ${styles.button} ${styles.primary} ${styles[props.buttonState]}`}
                style={props.additionalStyle || {}}
            />
        );
    } else {
        return (
            <div
                className={`${styles.button} ${styles[props.type]} ${styles[props.buttonState]} button`}
                style={props.additionalStyle || {}}
                onClick={() => {
                    if (props.action !== undefined) {
                        props.action();
                    } else {
                        return;
                    }
                }}
            >
                {props.title}
            </div>
        )
    }


}

export default Button
