import React, { useEffect, useRef, useState } from "react";
import { Link } from "@reach/router";
import { withPrefix } from "gatsby-link";

import { v4 as uuidv4 } from "uuid";

import {
    getLan,
    setTranslationConfig,
    tStatic,
} from "../../../services/multilingual/translate";

import * as styles from "./styles.module.scss";

import LanguageIcon from "../../../images/icon_language.svg";
import ChevronIcon from "./chevron.svg";

const LanguageSwitcher = ({ type, path, white, extraClass, menu, setMenu, lang }) => {
    let langs = [
        { id: "de", slug: "DE", string: "Deutsch" },
        { id: "en", slug: "EN", string: "English" },
    ];

    const [open, setOpen] = useState(false);
    const ref = useRef();

    useEffect(() => {

        const checkIfClickedOutside = e => {

            if(open && !ref.current.contains(e.target)){
                setOpen(false)
            }
        }

        document.addEventListener("mousedown", checkIfClickedOutside)

        return () => {
            document.removeEventListener("mousedown", checkIfClickedOutside)
        }

    }, [open])

    const renderLanguages = () => {
        return langs.map((_lang) => {
            return (
                <li
                    key={uuidv4()}
                    className={`${_lang.id === lang && styles.active} ${_lang.id === lang &&
                        (extraClass != undefined ? extraClass.active : "")
                        }`}
                >
                    <Link
                        onClick={() => {
                            if (type == "mobileMenu") {
                                setMenu(!menu);
                            }
                        }}
                        to={withPrefix(`${_lang.id !== "de" ? "/" + _lang.id : ""}${path}`)}
                        onClick={() => changeLanguage(_lang.id)}
                    >
                        {type == "second" ? _lang.string : _lang.slug}
                    </Link>
                </li>
            );
        });
    };

    const changeLanguage = (lanId) => {
        localStorage.setItem("lan", lanId);
        setTranslationConfig(lanId);
    };

    return (
        <div
            className={`${type == "mobileMenu" ? styles.languageSwitcherMobile : styles.languageSwitcher}  ${styles[type]
                } ${white ? styles.white : null} ${open ? styles.open : null}`}
                ref={ref}
        >
            {type != "mobileMenu" && (
                <LanguageIcon className={`${styles.icon} ${styles[type]}`} onClick={() => setOpen(prev => (!prev))} />
            )}
            {type == "second" ? (
                <div className={styles.label} onClick={() => setOpen(prev => (!prev))}>{tStatic("change_language", lang)}</div>
            ) : (
                ""
            )}
            {
                (open || type == "mobileMenu") && <ul
                    className={`${extraClass != undefined ? extraClass.secondMenu : ""} ${styles.items
                        }`}
                >
                    {renderLanguages()}
                </ul>
            }

            {
                type !== 'mobileMenu' &&

                <ChevronIcon
                    className={styles.chevron}
                    style={{ position: "relative", top: "3px", left: "10px" }}
                />
            }
        </div>
    );
};

export default LanguageSwitcher;
