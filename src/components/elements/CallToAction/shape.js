import React, { useEffect, useRef, useState } from "react";

const CTAShape = ({ wrapper, className }) => {
    const [coordinates, setCoordinates] = useState("");
    const [offset, setOffset] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);
    const [height, setHeight] = useState(915);
    const [isMobile, setIsMobile] = useState(false);

    const onResize = () => {
        let bottom = 864;
        let width = document.documentElement.clientWidth;


        if (width < 992) {

            let offsetRight = 70;
            bottom = 804;
            setHeight(840);

            if (width < 768) {
                offsetRight = 40;
                bottom = 450;
                setHeight(470);
                setIsMobile(true);
            } else {
                setIsMobile(false);
            }

            setCoordinates(`M0 0V${bottom}S${(width / 4 * 2)},${bottom + 50},${width},${bottom - offsetRight}V0Z`);
        } else {

            setIsMobile(false);
            setHeight(915);

            setCoordinates(
                `M0 0H${width}V${bottom}S${(width / 4) * 3},${bottom - 50},${width / 2
                },${bottom}S${width / 7},${bottom + 30},${0},${bottom - 50}Z`,
            );
        }

        setWindowWidth(document.documentElement.clientWidth);



        // setCoordinates(
        // 	`M0,0H${width}V${bottom}.4s-259-91.794-538-67.482C970.59,872.834,817.432,902.445,538,904.4,248.767,906.423,0,832.918,0,832.918Z`,
        // );
    };

    useEffect(() => {
        onResize();
        window.addEventListener("resize", onResize);

        return () => {
            window.removeEventListener("resize", onResize);
        };
    }, []);

    // const width = right + 50 * 100; //366.478;

    return (
        <>
            <svg
                className={className}
                xmlns="http://www.w3.org/2000/svg"
                width={windowWidth}
                height={height}
                viewBox={`0 0 ${windowWidth} ${height}`}
            >
                <defs>

                    {
                        isMobile ?
                            <linearGradient
                                id="cta-shape-gradient"
                                x1={0.5}
                                x2={0.5}
                                y2={1}
                                gradientUnits="objectBoundingBox"
                            >
                                <stop offset="0" stopColor="#fe5000" />
                                <stop offset="0.5" stopColor="#fe5000" />
                                <stop offset="1" stopColor="#fb8f22" />
                            </linearGradient>
                            :
                            <linearGradient
                                id="cta-shape-gradient"
                                x2={1}
                                y2={1}
                                gradientUnits="objectBoundingBox"
                            >

                                <>
                                    <stop offset="0" stopColor="#fb8f22" />
                                    <stop offset="0.478" stopColor="#fe5000" />
                                    <stop offset="1" stopColor="#fe5000" />
                                </>
                            </linearGradient>
                    }

                    <filter
                        id="Path_CTA_Shape"
                        x="0"
                        y="0"
                        width={windowWidth}
                        height={height}
                        filterUnits="userSpaceOnUse"
                    >
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood floodOpacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                    </filter>
                </defs>
                <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#Path_CTA_Shape)">
                    <path
                        id="Pfad_CTA_SHAPE_1"
                        data-name="Pfad 604"
                        d={coordinates}
                        fill="url(#cta-shape-gradient)"
                    />
                </g>
            </svg>

            {/* <div style={{ width: '100vw', position: 'fixed', top: 0, left: 0, height: '100vh', display: 'inline-block', textAlign: 'center' }}>
                <div style={{ width: 1100, height: 1000, display: 'inline-block', border: '1px solid rgba(0,0,0,0.1)' }}></div>
            </div> */}
        </>
    );
};

export default CTAShape;
