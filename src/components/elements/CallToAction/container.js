import React, { useRef } from "react";

import renderElements from "../index";
import CTAShape from "./shape";

import * as styles from "./styles.module.scss";

const CallToActionContainer = (props) => {
	const ref = useRef();
	let children = props.children;

	children.forEach((element) => {
		if (element.id === "button") element.type = "secondary";
	});

	return (
		<div className={styles.callToAction} ref={ref}>
			<CTAShape className={styles.shape} wrapper={ref} />
			<div className="container">
				<div style={{ width: "100%" }}>
					{renderElements(children, props.lang)}
				</div>
			</div>
		</div>
	);
};

export default CallToActionContainer;
