import React, { useEffect, useState } from "react";
import * as styles from "./styles.module.scss";
import getVerticalScrollPercentage from "../../../func/pagePosInPercent";

const ScrollLineBar = () => {
	const [height, setHeight] = useState(0);

	const listener = () => {
		setHeight(getVerticalScrollPercentage());
	};

	useEffect(() => {
		listener();
		document.addEventListener("scroll", listener);
		return () => {
			document.removeEventListener("scroll", listener);
		};
	}, []);

	return (
		<div
			style={{ height: height + "%" }}
			className={styles.scrollLineBar}
		></div>
	);
};

export default ScrollLineBar;
