import React, { useEffect, useState } from "react";
import Button from "../Button";
import * as styles from "./styles.module.scss";

const ImageLogoContainer = (props) => {

    return (
        <div className={styles.container}>
            <img
                src={props.image_url}
                className={styles.img}
            />
            <img
                src={props.logo_url}
                className={styles.logo}
            />
        </div>
    );
};

export default ImageLogoContainer;
