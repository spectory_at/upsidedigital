import React, { useEffect, useRef, useState } from "react";
import * as styles from "./styles.module.scss";

const ScrollDownButton = ({ label, destination }) => {

    let scrollTo = 0;

    if (destination === '#second-animation') {
        scrollTo = 230;
    } else if (destination === '#down') {
        if (typeof window !== "undefined") {
            scrollTo = window.innerHeight;
        }
    } else {

        if (typeof window !== "undefined") {
            let destinationElement = document.getElementById(destination.replace('#', ''));


            if (destinationElement !== null) {
                scrollTo = destinationElement.offsetTop - window.innerHeight - 100;
            }

        }

    }


    return (
        <div className={styles.animationWrapper}
            onClick={() => {

                window.scrollTo({
                    top: scrollTo,
                    left: 0,
                    behavior: 'smooth'
                });
            }}
        >
            <div className={styles.animationContainer}>
                <svg className={styles.arrow} xmlns="http://www.w3.org/2000/svg" width="21.547" height="12.274" viewBox="0 0 21.547 12.274" >
                    <path id="Pfad_266" data-name="Pfad 266" d="M18.613-3,9.96,5.653,1.308-3" transform="translate(20.734 7.153) rotate(180)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" />
                </svg >
                <svg className={styles.arrow} xmlns="http://www.w3.org/2000/svg" width="21.547" height="12.274" viewBox="0 0 21.547 12.274" >
                    <path id="Pfad_266" data-name="Pfad 266" d="M18.613-3,9.96,5.653,1.308-3" transform="translate(20.734 7.153) rotate(180)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" />
                </svg >
                <svg className={styles.arrow} xmlns="http://www.w3.org/2000/svg" width="21.547" height="12.274" viewBox="0 0 21.547 12.274" >
                    <path id="Pfad_266" data-name="Pfad 266" d="M18.613-3,9.96,5.653,1.308-3" transform="translate(20.734 7.153) rotate(180)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" />
                </svg >
            </div>
            <div className={styles.label}>{label}</div>
        </div>
    )

}

export default ScrollDownButton;