import React from 'react';
import * as styles from './blogposts.module.scss';
import parse from 'html-react-parser';
import Button from '../Button';
import { Link } from 'gatsby';
const BigPost = (props) => {

    const image = props.featuredImage?.node.sourceUrl;
    const title = props.title;
    const dateDe = props.dateDe;
    const dateEn = props.dateEn;
    const readingLength = Math.floor(props.content.split(/\s+/).filter(word => word.length > 0).length / 230);
    const excerpt = props.excerpt;
    const linkToPost = props.link.replace('blog/de/', 'blog/');
    const lang = props.locale.locale;

    return <div id={props.id} className={styles.bigPost}>
        <Link className={styles.imageLink} to={linkToPost}>
            {image ?
                <img className={styles.image} src={image} alt={title} /> :
                <div className={styles.placeholderImage}></div>
            }
        </Link>
        <div className={styles.infosWrapper}>
            <span className={styles.infos}>{lang.startsWith('en') ? dateEn : dateDe} | {lang.startsWith('en') ? "Reading time:" : 'Lesezeit:'} {readingLength} min </span>
            <Link to={linkToPost}><h3>{title}</h3></Link>
            {parse(excerpt)}
            <Button title={lang.startsWith('en') ? 'Read article' : 'Beitrag lesen'} destination={linkToPost} type="primary" />
        </div>
    </div>
}

export default BigPost;