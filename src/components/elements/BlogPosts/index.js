import React, { useEffect, useState } from "react"
import * as styles from "./blogposts.module.scss"
import { graphql, useStaticQuery } from "gatsby"
import BigPost from "./big";
import SmallPost from "./small";
import Button from "../Button";

const BlogPosts = (props) => {

    const posts_unfiltered = useStaticQuery(graphql`
    {
        allWpPost(sort: {order: DESC, fields: date}) {
          nodes {
            title
            content
            dateDe: date(formatString: "DD. MMMM YYYY", locale: "de_De")
            dateEn: date(formatString: "DD. MMMM YYYY", locale: "en_Us")
            featuredImage {
              node {
                sourceUrl
              }
            }
            locale {
                locale
            }
            excerpt
            uri
          }
        }
      }
    `);

    const [shownSmall, setShownSmall] = useState(3);

    const language = props.lang;
    let posts_filtered = posts_unfiltered.allWpPost.nodes.filter((p) => p.locale.locale.startsWith(language));
    const linkBase = `/${language === 'en' ? 'en/' : ''}blog`;
    let posts_split = [];
    if (posts_filtered.length >= 3) {
        posts_split.push(posts_filtered.slice(0, 3));
        posts_split.push(posts_filtered.slice(3));
    } else {
        posts_split.push(posts_filtered);
        posts_split.push([]);
    }

    const showMore = () => {
        if (posts_split[1].length - shownSmall <= 3) {
            setShownSmall(posts_split[1].length);
        } else {
            setShownSmall(prev => prev + 3);
        }
    }

    const EmptyMessage = () => {
        const message = [
            "Currently, we have no posts to read. Please check back another time!",
            "Aktuell haben wir keine Beiträge zum Lesen. Bitte schauen Sie später vorbei!"
        ];

        return (
            <div className={styles.emptyMessage}>
                <h2>{language === 'en' ? message[0] : message[1]}</h2>
            </div>
        );
    }

    return (
        <>
            {posts_split[0].length > 0 ?
                posts_split[0].map((p, i) => <BigPost id={i == 0 ? 'posts' : ''} {...p} link={`${linkBase}${p.uri}`} />) : <EmptyMessage />
            }

            {posts_split[1].length > 0 ?
                <>
                    {<SplitHeadlines subheadline={props.subheadline} headline={props.headline} lang={language} />}
                    <div className={styles.smallPostsWrapper}>
                        {posts_split[1].slice(0, shownSmall).map((p) => <SmallPost {...p} link={`${linkBase}${p.uri}`} />)}
                    </div>
                    {shownSmall < posts_split[1].length ?
                        <div className={styles.buttonWrapper}><Button onClick={showMore} type="primary" title={language.startsWith('en') ? "Show more" : 'Mehr anzeigen'} /></div>
                        : undefined}
                </>
                : undefined}
        </>
    );

}

const SplitHeadlines = ({subheadline = "", headline = "", lang}) => {

    if(subheadline === "") {
        subheadline = (lang == 'en' || lang.startsWith('en')) ? 'More Topics' : 'Weitere Themen';
    }

    if(headline === "") {
        headline = (lang == 'en' || lang.startsWith('en')) ? 'Might also interest you' : 'Könnte für Sie interessant sein';
    }

    return (
        <div className={styles.splitWrapper}>
            <span>{subheadline}</span>
            <h2>{headline}</h2>
        </div>
    );
}

export { BlogPosts as default, SplitHeadlines}
