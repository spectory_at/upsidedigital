import React from 'react';
import * as styles from './blogposts.module.scss';
import parse from 'html-react-parser';
import { Link } from 'gatsby-link';

const SmallPost = (props) => {

    const image = props.featuredImage?.node.sourceUrl;
    const title = props.title;
    const readingLength = Math.floor(props.content.split(/\s+/).filter(word => word.length > 0).length / 230);
    const excerpt = props.excerpt;
    const linkToPost = props.link;
    const lang = props.locale.locale;

    return <div className={styles.smallPost}>
        <div className={styles.imageWrapper}>
            {image ? <Link to={linkToPost} ><img src={image} className={styles.image} alt={title} /></Link> : <div className={styles.placeholderImage}></div>}
        </div>
        <div className={styles.content}>
            <span className={styles.infos}>{lang.startsWith('en') ? "Reading time:" : 'Lesezeit:'} {readingLength} min</span>
            <Link to={linkToPost}><h3>{title}</h3></Link>
            {parse(excerpt)}
        </div>
    </div>
}

export default SmallPost;