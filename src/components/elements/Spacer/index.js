import React, { useEffect, useRef, useState } from "react"
import * as styles from "./styles.module.scss"

const Spacer = props => {

    const [responsiveHeight, setResponsiveHeight] = useState({});

    const calcStyles = (e) => {
        const windowWidth = document.documentElement.clientWidth;

        let height = props.style.height;

        if (windowWidth <= 1500) {
            if (props.style.heightDesktop !== '' && props.style.heightDesktop !== null) {
                height = props.style.heightDesktop;
            }
        }

        if (windowWidth <= 992) {
            if (props.style.heightTablet !== '' && props.style.heightTablet !== null) {
                height = props.style.heightTablet;
            }
        }

        if (windowWidth <= 768) {
            if (props.style.heightMobile !== '' && props.style.heightMobile !== null) {
                height = props.style.heightMobile;
            }
        }

        setResponsiveHeight(height);

    }

    useEffect(() => {
        calcStyles();
        window.addEventListener('resize', calcStyles);

        return () => {
			document.removeEventListener("resize", calcStyles);
		};
    }, [])

    return (
        <div style={{ height: responsiveHeight }} className={styles.container}></div>
    )


}


export default Spacer;