import React, {useEffect, useRef} from 'react';
import * as styles from "./styles.module.scss"

const TestimonialsItem = ({text, image_url}) => {
    return (
        <div className={`testimonials_item ${styles.item}`}>
            <div className={styles.text}>
                {text}
            </div>
            <img width="50px" height="50px" src={image_url}/>
        </div>
    )
};

export default TestimonialsItem;
