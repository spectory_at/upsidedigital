import React, { useEffect, useRef, useState } from "react";
import renderElements from "../index";
import elementInViewport from "../../../func/elementInViewPort";
import animateCSS from "../../../func/animateCSS";
import * as styles from "./styles.module.scss";
const TestimonialsContainer = ({ children, style, lang }) => {
	const ref = useRef();
	let eStyle = {};
	const [items, setItems] = useState([]);
	if (style.position == "right") {
		eStyle.float = "right";
		// eStyle.overflow = "hidden";
		eStyle.paddingLeft = "10px";
		eStyle.paddingTop = "10px";
		eStyle.overflow = "hidden";
	}

	useEffect(() => {
		let animatedLocal = false;

		const check = () => {
			if (ref.current) {
				if (elementInViewport(ref.current)) {
					if (!animatedLocal) {
						let testimonialsItems =
							document.querySelectorAll(".testimonials_item");
						let delay = 1.5;
						testimonialsItems.forEach((e, i) => {
							delay += 0.2;
							e.style.setProperty("--animate-duration", delay + "s");
							ref.current.classList.add(styles.show);
							animateCSS(e, "bounceInUp").then((message) => {});
						});

						animatedLocal = true;
					}
				}
			}
		};

		check(ref);

		document.addEventListener(
			"scroll",
			() => {
				check(ref);
			},
			[],
		);
	}, []);

	return (
		<div ref={ref} className={styles.container} style={eStyle}>
			{renderElements(children, lang)}
		</div>
	);
};

export default TestimonialsContainer;
