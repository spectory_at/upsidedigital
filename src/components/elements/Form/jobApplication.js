import React, { useState } from 'react';
import * as styles from "./styles.module.scss"
import { t, tStatic } from "../../../services/multilingual/translate"
import { useForm } from 'react-hook-form';
import { API_BASE } from '../../../spectory-config';
import axios from 'axios';

import SuccessIcon from "../../../images/whiteDot2.svg";

const JobApplicationForm = ({ style, lang }) => {

    let eStyles = {}

    if (style.position == "right") {
        eStyles.float = "right";
    }

    const [loading, setLoading] = useState(false);
    const [sendingSuccess, setSendingSuccess] = useState(false);
    const [file, setFile] = useState(null);
    const [fileName, setFileName] = useState(null);
    
    const { register, handleSubmit, reset, formState, watch } = useForm();
    const [theForm, setTheForm] = useState([]);

    const [showErrors, setShowErrors] = useState(false);

    const [errors, setErrors] = useState({
        name: true,
        email: true,
        file: true,
    });

    watch((data, { name, type }) => {

        let _errors = { ...errors }

        if (_errors[name] !== undefined) {
            _errors[name] = data[name] === '';
            setErrors(_errors)
        }

    })
    const onSubmit = (data) => {

        setShowErrors(false);
        let valid = true;

        Object.keys(errors).forEach(key => {
            if (errors[key] === true) {
                setShowErrors(true);
                valid = false;
            }
        });

        if (!valid) return;
        setLoading(true);


        var formData = new FormData();
        formData.append("name", data.name);
        formData.append("email", data.email);
        formData.append("job", 'initiativ');
        formData.append("file", file);

        axios.post(API_BASE + '/apply_job', formData).then(res => {
            setLoading(false);
            setSendingSuccess(true);
            setFile(null);
            setFileName(null);

            setTimeout(() => {
                setSendingSuccess(false);
            }, 5000);

            reset();
        }).catch(err => {
            console.error(err);
            setLoading(false);
        })

        setLoading(true);
    }

    const updateFile = (e) => {
        setFile(e.target.files[0]);


        // CALC FILENAME

        // MAX 25 in total

        let _fileName = e.target.files[0].name;
        let length = _fileName.length;

        let suffix = '';
        let _prefix = '';

        if(_fileName.includes('.')){
            suffix = _fileName.split('.')[1];
            _prefix = _fileName.split('.')[0];
        }

        let maxLength = 25;
        if(document.documentElement.clientWidth < 768){
            maxLength = 17;
        }

        let suffixLength = suffix.length;
        let prefixLength = maxLength - suffixLength - 1; // 1 = for .

        let prefix = _prefix;

        if(_prefix.length > prefixLength){
            let firstPart = _prefix.substring(0, prefixLength - 7)
            let secondPart = _prefix.substring(_prefix.length - 4)

            prefix = firstPart+'...'+secondPart;
        }

        
        setFileName(prefix+'.'+suffix);

        let _errors = { ...errors }

        if (_errors.file !== undefined) {
            _errors.file = false;
            setErrors(_errors)
        }
    }

    return (
        <div style={eStyles} className={`${styles.form} ${loading ? styles.loading : null} ${styles.applicationForm}`}>
            <div className={styles.labelWrapper} y={[0, -60]} tagOuter="figure">
                <div className={`${styles.label} `}>
                    {tStatic('looking_forward_to_meed', lang)}
                </div>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.inputGroup}>
                    <label>
                        {tStatic("name_label", lang)}
                        <span>*</span>
                    </label>
                    <input
                        {...register("name")} type="text"
                        placeholder={errors.name && showErrors ? tStatic("please_fill_out", lang) : tStatic("name", lang)}
                        className={errors.name && showErrors ? styles.error : ''}
                    />
                </div>
                <div className={styles.inputGroup}>
                    <label>
                        {tStatic("email_label", lang)}
                        <span>*</span>
                    </label>
                    <input
                        {...register("email")}
                        type="email"
                        placeholder={errors.email && showErrors ? tStatic("please_fill_out", lang) : tStatic("email", lang)}
                        className={errors.email && showErrors ? styles.error : ''}
                    />
                </div>
                <div className={styles.inputGroup}>
                    <label>
                        {tStatic("cv", lang)}
                        <span>*</span>
                    </label>
                    <input
                        id="file"
                        name="file"
                        {...register("file")}
                        onChange={e => updateFile(e)} type="file" placeholder={tStatic('cv', lang)}
                        placeholder={errors.file && showErrors ? tStatic("please_fill_out", lang) : tStatic("file", lang)}
                        className={errors.file && showErrors ? styles.file : ''}
                    />
                    <label htmlFor="file" className={`${styles.fileLabel} ${file !== null ? styles.selected : null} ${errors.file && showErrors ? styles.error : ''}`}>{file !== null ? fileName : (errors.file && showErrors ? tStatic("please_fill_out", lang) : tStatic('cv_placeholder', lang))}</label>

                </div>
                <button className={styles.button} type="submit">{tStatic('send_application', lang)}</button>

            </form>
            {
                sendingSuccess && <div className={styles.success}><SuccessIcon />{tStatic("success_sending_form", lang)}</div>
            }
        </div>
    );
};

export default JobApplicationForm;
