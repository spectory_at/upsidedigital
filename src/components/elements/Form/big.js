import React, { useState } from 'react';
import * as styles from "./styles.module.scss"
import { t, tStatic } from "../../../services/multilingual/translate"
import { useForm } from 'react-hook-form';
import { API_BASE } from '../../../spectory-config';
import axios from 'axios';

import SuccessIcon from "../../../images/whiteDot2.svg";
import { capiGetLead } from '../../../services/capi';

const BigForm = ({ style, lang }) => {

    let eStyles = {}

    if (style.position == "right") {
        eStyles.float = "right";
    }

    const [loading, setLoading] = useState(false);
    const [sendingSuccess, setSendingSuccess] = useState(false);

    const { register, handleSubmit, reset, formState, watch } = useForm();
    const [theForm, setTheForm] = useState([]);

    const [showErrors, setShowErrors] = useState(false);

    const [errors, setErrors] = useState({
        email: true,
        message: true,
    });

    watch((data, { name, type }) => {

        let _errors = { ...errors }

        if (_errors[name] !== undefined) {
            _errors[name] = data[name] === '';
            setErrors(_errors)
        }

    })
    const onSubmit = (data) => {

        setShowErrors(false);
        let valid = true;

        Object.keys(errors).forEach(key => {
            if (errors[key] === true) {
                setShowErrors(true);
                valid = false;
            }
        });

        if (!valid) return;

        axios.post(API_BASE + '/contact', data).then(res => {
            setLoading(false);
            setSendingSuccess(true);

            setTimeout(() => {
                setSendingSuccess(false);
            }, 5000);

            reset();
        }).catch(err => {
            console.error(err);
            setLoading(false);
        })

        setLoading(true);

        capiGetLead();
    }

    return (
        <div style={eStyles} className={`${styles.form} ${loading ? styles.loading : null} ${styles.big}`}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.inputGroup}
                    style={{ marginBottom: 0 }}
                >
                    <label>{tStatic("email_label", lang)}<span>*</span></label>
                    <input
                        style={{ marginBottom: 30 }}
                        placeholder={errors.email && showErrors ? tStatic("please_fill_out", lang) : tStatic("email", lang)}
                        className={errors.email && showErrors ? styles.error : ''}
                        {...register("email")} type="email"
                    />
                </div>
                <div className={styles.inputGroup}>
                    <label
                        style={{ marginTop: 0 }}
                    >{tStatic("how_can_we_help", lang)}<span>*</span></label>
                    <textarea
                        style={{ marginBottom: 30 }}
                        {...register("message")}
                        placeholder={errors.message && showErrors ? tStatic("please_fill_out", lang) : tStatic("how_can_we_help_placeholder", lang)}
                        className={errors.message && showErrors ? styles.error : ''}

                    ></textarea>

                    <button className={styles.button} style={{ float: 'right', padding: "0px 40px" }} type="submit">{tStatic('send_message', lang)}</button>
                </div>

            </form>
            {
                sendingSuccess && <div className={styles.success}><SuccessIcon />{tStatic("success_sending_form", lang)}</div>
            }
        </div>
    );
};

export default BigForm;
