import React, { useState } from 'react';
import * as styles from "./styles.module.scss"
import { t, tStatic } from "../../../services/multilingual/translate"
import { useForm } from 'react-hook-form';
import { API_BASE } from '../../../spectory-config';
import axios from 'axios';

import SuccessIcon from '../../../images/check.svg';

const BigForm = ({ style, lang }) => {

    let eStyles = {}

    if (style.position == "right") {
        eStyles.float = "right";
    }

    const [loading, setLoading] = useState(false);
    const [sendingSuccess, setSendingSuccess] = useState(false);

    const { register, handleSubmit, reset } = useForm();

    const onSubmit = (data) => {

        axios.post(API_BASE + '/contact', data).then(res => {
            setLoading(false);
            setSendingSuccess(true);

            setTimeout(() => {
                setSendingSuccess(false);
            }, 5000);

            reset();
        }).catch(err => {
            console.error(err);
            setLoading(false);
        })

        setLoading(true);
    }

    return (
        <div style={eStyles} className={`${styles.form} ${loading ? styles.loading : null} ${styles.big}`}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <h2>{tStatic('contact_form', lang)}</h2>
                <div className="container fluid" style={{ padding: 0 }}>
                    <div className="col6">
                        <input required {...register("name")} type="text" placeholder={tStatic('name', lang)} />
                    </div>
                    <div className="col6">
                        <input required {...register("email")} type="email" placeholder={tStatic('email', lang)} />
                    </div>
                </div>
                <div className="container fluid" style={{ padding: 0 }}>
                    <div className="col6">
                        <input required {...register("phone")} type="text" id="phone" placeholder={tStatic('phone', lang)} />
                    </div>
                    <div className="col6">
                        <input required {...register("url")} type="text" id="url" placeholder={tStatic('url', lang)} />
                    </div>
                </div>
                <div className="container fluid" style={{ padding: 0 }}>
                    <div className="col12">
                        <input required {...register("subject")} type="text" id="subject" placeholder={tStatic('subject', lang)} />
                    </div>
                </div>
                <div className="container fluid" style={{ padding: 0 }}>
                    <div className="col12">
                        <textarea required {...register("message")} placeholder={tStatic('message', lang)}></textarea>
                    </div>
                </div>

                <button className={styles.button} type="submit">{tStatic('send_message', lang)}</button>

            </form>
            {
                sendingSuccess && <div className={styles.success}><SuccessIcon />{tStatic("success_sending_form", lang)}</div>
            }
        </div>
    );
};

export default BigForm;
