import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import parse from "html-react-parser";

import { t, tStatic } from "../../../services/multilingual/translate";
import { API_BASE } from "../../../spectory-config";

import * as styles from "./styles.module.scss";

import SuccessIcon from "../../../images/whiteDot2.svg";
import { capiGetLead } from "../../../services/capi";

const SmallForm = ({ style, lang }) => {
    let eStyles = {};

    if (style.position == "right") {
        eStyles.float = "right";
    }

    const [loading, setLoading] = useState(false);
    const [sendingSuccess, setSendingSuccess] = useState(false);

    const { register, handleSubmit, reset, formState, watch } = useForm();
    const [theForm, setTheForm] = useState([]);

    const [showErrors, setShowErrors] = useState(false);

    const [errors, setErrors] = useState({
        name: true,
        email: true,
        phone: true,
    });

    watch((data, { name, type }) => {

        let _errors = { ...errors }

        if (_errors[name] !== undefined) {
            _errors[name] = data[name] === '';
            setErrors(_errors)
        }

    })


    const onSubmit = (data) => {

        setShowErrors(false);
        let valid = true;

        Object.keys(errors).forEach(key => {
            if (errors[key] === true) {
                setShowErrors(true);
                valid = false;
            }
        });

        if(!valid) return;

        axios
            .post(API_BASE + "/contact", data)
            .then((res) => {
                setLoading(false);
                setSendingSuccess(true);

                setTimeout(() => {
                    setSendingSuccess(false);
                }, 5000);

                reset();
            })
            .catch((err) => {
                setLoading(false);
            });

        setLoading(true);

        capiGetLead();
    };


    return (
        <div
            style={eStyles}
            className={`${styles.form} ${style.position == "hero" ? "hero-small-form" : ""
                } ${styles.small} `}
        >
            <div className={styles.labelWrapper} y={[0, -60]} tagOuter="figure">
                <div className={`${styles.label} `}>
                    {parse(tStatic("free_trial", lang))}
                </div>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.inputGroup}>
                    <label>
                        {tStatic("name_label", lang)}
                        <span>*</span>
                    </label>
                    <input
                        type="text"
                        {...register("name", { required: "" })}
                        placeholder={errors.name && showErrors ? tStatic("please_fill_out", lang) : tStatic("name", lang)}
                        className={errors.name && showErrors ? styles.error : ''}
                    />
                </div>
                <div className={styles.inputGroup}>
                    <label>{tStatic("company_label_form", lang)}</label>
                    <input
                        type="text"
                        {...register("company")}
                        placeholder={tStatic("company", lang)}
                    />
                </div>
                <div className={styles.inputGroup}>
                    <label>
                        {tStatic("email_label", lang)}
                        <span>*</span>
                    </label>
                    <input
                        type="email"
                        {...register("email")}
                        placeholder={errors.email && showErrors ? tStatic("please_fill_out", lang) : tStatic("email", lang)}
                        className={errors.email && showErrors ? styles.error : ''}
                    />
                </div>
                <div className={styles.inputGroup}>
                    <label>
                        {tStatic("phone_label", lang)}
                        <span>*</span>
                    </label>
                    <input
                        type="text"
                        {...register("phone")}
                        placeholder={errors.phone && showErrors ? tStatic("please_fill_out", lang) : tStatic("phone", lang)}
                        className={errors.phone && showErrors ? styles.error : ''}
                    />
                </div>
                <button className={styles.button} type="submit">
                    {tStatic("request_recall", lang)}
                </button>
            </form>
            {sendingSuccess && (
                <div className={styles.success}>
                    <SuccessIcon />
                    {tStatic("recall_requested", lang)}
                </div>
            )}
        </div>
    );
};

export default SmallForm;
