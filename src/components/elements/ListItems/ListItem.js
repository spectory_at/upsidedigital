import React from "react";
import * as styles from "./listItems.module.scss";

const ListItem = ({ text, style }) => {
	return (
		<li
			className={`${style.item_style == "white" ? styles.listItemWhite : ""} ${
				styles.listItem
			}`}
		>
			{text}
		</li>
	);
};

export default ListItem;
