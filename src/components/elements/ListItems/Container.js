import React from 'react';
import * as styles from "./listItems.module.scss"
import renderElements from "../index";

const ListItemsContainer = ({ children, lang }) => {
    return (
        <ul className={styles.listItems}>
            {renderElements(children, lang)}
        </ul>
    );
};

export default ListItemsContainer;
