import React from "react";
import { Link, withPrefix } from "gatsby";

import * as styles from "../../scss/footer.module.scss";
import LanguageSwitcherIcon from "../../images/languageSwitcherIcon.svg";
//
import Social from "../elements/Social";
import LanguageSwitcher from "../elements/LanguageSwitcher";
import { tStatic } from "../../services/multilingual/translate";
import BackgroundShapeMobile from "../elements/StartHero/BackgroundShapeMobile";

const Footer = ({ lang, path }) => {
	const basePath = lang !== "de" ? "/" + lang : "";

	return (
		<div className={styles.footer}>
			<div
				className={`container ${styles.container}`}
				style={{ alignItems: "center", paddingTop: "15px" }}
			>
				<div className="col7">
					<div className={styles.menu}>
						<Link to={`${basePath}/contact`}>{tStatic("contact", lang)}</Link>
						<Link to={`${basePath}/privacy-policy`}>
							{tStatic("data_protection", lang)}
						</Link>
						<Link to={`${basePath}/imprint`}>
							{tStatic("legal_notice", lang)}
						</Link>
						<LanguageSwitcher lang={lang} type="second" path={path} />
					</div>
				</div>
				<div
					className="col5"
					style={{ display: "flex", alignItems: "flex-end" }}
				>
					<Social />
				</div>
			</div>

			<div className={`container ${styles.bottomFooter}`}>
				<div className="col12">
					&copy; Copyright 2024 Upside Digital GmbH. {tStatic('all_rights_reserved', lang)}
				</div>
			</div>
		</div>
	);
};

export default Footer;
