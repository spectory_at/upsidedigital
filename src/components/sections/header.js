import React, { useEffect, useState } from "react";
import { Link } from "gatsby";

import { t, tStatic } from "../../services/multilingual/translate";

import Button from "../elements/Button";
import Burger from "../elements/Burger";

import * as styles from "../../scss/header.module.scss";
import { HOST } from "../../spectory-config";

const Header = ({
    lang,
    setMenu,
    menu,
    path,
    activeDefault,
    white,
    hideMenu,
}) => {
    if (hideMenu == "true") {
        hideMenu = true;
    } else if (hideMenu == "false") {
        hideMenu = false;
    }
    const [active, setActive] = useState(activeDefault);
    const [isWhite, setIsWhite] = useState(white === undefined ? false : white);

    if (typeof window !== "undefined" && !activeDefault) {
        window.addEventListener("scroll", () => {
            if (path == "/") {
                if (window.scrollY > 350) {
                    if (!active) setActive(true);
                } else {
                    if (active) setActive(false);
                }
            } else {
                if (window.scrollY > 100) {
                    if (!active) setActive(true);
                } else {
                    if (active) setActive(false);
                }
            }

        });
    }

    useEffect(() => {
        if(white){
            setIsWhite(!menu);
        }
    }, [])



    let langBaseUrl = "";

    if (lang !== "de") {
        langBaseUrl = "/" + lang;
    }

    return (
        <div
            className={`${styles.header} ${active ? styles.active : styles.inactive} ${menu ? styles.menu : styles.noMenu} ${isWhite ? styles.white : null}`}
        >
            <div className={`container ${styles.container}`}>
                <div>
                    <Link to={`${langBaseUrl}/`} className={styles.logo}>
                        <img
                            style={{ display: active || menu ? "inline-block" : "none" }}
                            src={`${HOST}/wp-content/uploads/2021/06/logoupsideorange@3x.png`}
                        />
                        <img
                            style={{ display: active || menu ? "none" : "inline-block" }}
                            src={`${HOST}/wp-content/uploads/2021/06/logoupsidewhite@3x.png`}
                        />
                    </Link>
                    {!hideMenu && !menu && (
                        <ul
                            className={`hide-mobile ${styles.menu}`}
                            style={{ marginLeft: "102px" }}
                        >
                            <li>
                                <Link
                                    className={`${styles.link} ${path === `/implementierung` ? styles.current : null} `}
                                    to={`${langBaseUrl}/implementierung`}
                                >
                                    {tStatic("implementierung", lang)}{" "}
                                </Link>
                            </li>
                            <li>
                                <Link
                                    className={`${styles.link} ${path === `/product` ? styles.current : null} `}
                                    to={`${langBaseUrl}/product`}>
                                    {tStatic("product", lang)}{" "}
                                </Link>
                            </li>
                            <li>
                                <Link
                                    className={`${styles.link} ${path === `/company` ? styles.current : null} `}
                                    to={`${langBaseUrl}/company`}
                                >
                                    {tStatic("company_label", lang)}{" "}
                                </Link>
                            </li>
                        </ul>
                    )}
                </div>
                <div>
                    {!hideMenu && (
                        <div className={styles.menuSecondary}>
                            <div style={{ marginRight: "30px" }} className="hide-mobile">
                                <Button
                                    destination={`${langBaseUrl}/landing-page`}
                                    title={tStatic("start_now", lang)}
                                    type={"primary"}
                                />
                            </div>
                            <Burger
                                active={active}
                                state={menu}
                                white={isWhite}
                                onClick={() => {
                                    setMenu(!menu);
                                }}
                            />
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default Header;
