/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import * as React from "react"
import PropTypes from "prop-types"
import { Helmet } from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"
import favicon180 from '../../images/apple-icon-180x180.png'
import favicon from '../../images/favicon.ico'


function Seo({ description, lang, meta, title, metaImageUrl, seoMetaTags }) {
    const { site } = useStaticQuery(
        graphql`
       query {
         site {
           siteMetadata {
             title
             description
             defaultImage
           }
         }
       }
     `
    )

    const metaDescription = seoMetaTags?.google.description || site.siteMetadata.description;
    const defaultTitle = site.siteMetadata?.title;
    const googleTitle = seoMetaTags?.google.title || title;


    const smTitle = seoMetaTags?.sm.title || title;
    const smDescription = seoMetaTags?.sm.description || metaDescription;
    const metaImage = seoMetaTags?.sm.image || site.siteMetadata.defaultImage;

    return (
        <Helmet
            htmlAttributes={{
                lang,
            }}
            title={googleTitle || defaultTitle}
            titleTemplate={defaultTitle && googleTitle !== '' ? `%s | ${defaultTitle}` : `%s`}
            meta={[
                {
                    name: `description`,
                    content: metaDescription,
                },
                {
                    property: `og:title`,
                    content: smTitle,
                },
                {
                    property: `og:description`,
                    content: smDescription,
                },
                {
                    property: `og:type`,
                    content: `website`,
                },
                {
                    property: `og:image`,
                    content: metaImage,
                },
                {
                    name: `twitter:card`,
                    content: `summary`,
                },
                {
                    name: `twitter:creator`,
                    content: site.siteMetadata?.author || ``,
                },
                {
                    name: `twitter:title`,
                    content: smTitle,
                },
                {
                    name: `twitter:description`,
                    content: smDescription,
                },
            ].concat(meta)}
            link={[
                { rel: 'icon', sizes: "180x180", type: 'image/png', href: `${favicon180}` },
                { rel: 'apple-touch-icon', type: 'image/png', href: `${favicon180}` },
                { rel: 'apple-touch-icon-precomposed', type: 'image/png', href: `${favicon180}` },

            ]}
        >
            <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon.png" />
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
            <link rel="manifest" href="/site.webmanifest" />
            <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#28b4e4" />
            <meta name="msapplication-TileColor" content="#28b4e4" />
            <meta name="theme-color" content="#ffffff" />
        </Helmet>
    )
}

Seo.defaultProps = {
    lang: `en`,
    meta: [],
    description: ``,
}

Seo.propTypes = {
    description: PropTypes.string,
    lang: PropTypes.string,
    meta: PropTypes.arrayOf(PropTypes.object),
    title: PropTypes.string.isRequired,
}

export default Seo
