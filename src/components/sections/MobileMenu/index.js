import React, { useEffect, useRef } from "react";
import { Link } from "gatsby";
import * as styles from "./styles.module.scss";
import * as stylesHeader from "../../../scss/header.module.scss";
import { tStatic } from "../../../services/multilingual/translate";
import LanguageSwitcher from "../../elements/LanguageSwitcher";
import Button from "../../elements/Button";
import Social from "../../elements/Social/index";

const MobileMenu = ({ menu, setMenu, lang, path, isMobile }) => {
    const ref = useRef();
    const basePath = lang !== "de" ? "/" + lang : "";
    useEffect(() => {
        setTimeout(() => {
            ref.current.classList.add(styles.active);
        }, 100);
    }, []);

    let langBaseUrl = "";

    if (lang !== "de") {
        langBaseUrl = "/" + lang;
    }


    return (
        <>
            <div className={styles.overlay}></div>
            <div ref={ref} className={styles.menu}>
                {/* <div className={styles.header}>
					<div className="container" style={{ width: "100%" }}>
						<div className={`col6 ${styles.col6}`}>
							{/* <Link to={`${langBaseUrl}/`}>
								<img
									src="https://spectory.at/upsidedigital/wp-content/uploads/2021/06/logoupsideorange@3x.png"
									className={`${stylesHeader.logo} ${styles.logo}`}
								/>
							</Link>
						</div>
					</div>
				</div> 
            */}

                <div className={styles.content}>
                    <div className={`container ${styles.container}`}>
                        <div className={isMobile ? "col4" : "col6"}>
                            <ul className={styles.items}>
                                <li className={path === "/" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/`}
                                    >
                                        {tStatic("homepage", lang)}
                                    </Link>
                                </li>
                                <li className={path === "/implementierung" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/implementierung`}
                                    >

                                        {tStatic("implementierung", lang)}
                                    </Link>
                                </li>
                                <li className={path === "/product" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/product`}
                                    >
                                        {tStatic("product", lang)}
                                    </Link>
                                </li>
                                <li className={path === "/references" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/references`}
                                    >
                                        {tStatic("references", lang)}
                                    </Link>
                                </li>
                                <li className={path === "/blog" ? styles.active: undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/blog`}
                                    >
                                        {tStatic("blog", lang)}
                                    </Link>
                                </li>
                                    
                            </ul>
                        </div>
                        <div className={isMobile ? "col4" : "col6"}>
                            <ul className={styles.items}>
                                <li className={path === "/faq" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/faq`}
                                    >
                                        {tStatic("faq", lang)}
                                    </Link>
                                </li>
                                <li className={path === "/company" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/company`}
                                    >
                                        {tStatic("company_menu", lang)}
                                    </Link>
                                </li>
                                <li className={path === "/karriere" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/karriere`}
                                    >
                                        {tStatic("career", lang)}
                                    </Link>
                                </li>
                                <li className={path === "/contact" ? styles.active : undefined}>
                                    <Link
                                        onClick={() => {
                                            setMenu(!menu);
                                        }}
                                        to={`${langBaseUrl}/contact`}
                                    >
                                        {tStatic("contact", lang)}
                                    </Link>
                                </li>

                            </ul>
                        </div>
                        {isMobile &&
                            <div className={styles.footerContainer} style={{ marginBottom: 50 }}>

                                <div className={styles.footerContainerItem}>
                                    <Button
                                        destination={`${langBaseUrl}/landing-page`}
                                        title={tStatic("startNow", lang)}
                                        onClick={() => setMenu(!menu)}
                                        type="primary"
                                    />
                                </div>
                                <div className={styles.footerContainerItem}>
                                    <LanguageSwitcher
                                        menu={menu}
                                        setMenu={setMenu}
                                        path={path}
                                        lang={lang}
                                        type="mobileMenu"
                                        extraClass={{
                                            secondMenu: styles.secondMenu,
                                            active: styles.active,
                                        }}
                                    />
                                </div>
                            </div>
                        }
                    </div>
                </div>
                {!isMobile &&
                    <div className={styles.footer}>
                        <div className="container" style={{ width: "100%" }}>
                            <div className={`col6 ${styles.col6}`}>
                                <Social />
                                <div className={styles.footerMenu}>
                                    <Link to={`${basePath}/privacy-policy`}>
                                        {tStatic("data_protection", lang)}
                                    </Link>
                                    <Link to={`${basePath}/imprint`}>
                                        {tStatic("legal_notice", lang)}
                                    </Link>
                                </div>
                            </div>
                            <div className={`col6 ${styles.col6}`}>
                                <div style={{ float: "right" }}>
                                    <LanguageSwitcher
                                        menu={menu}
                                        setMenu={setMenu}
                                        path={path}
                                        lang={lang}
                                        type="mobileMenu"
                                        extraClass={{
                                            secondMenu: styles.secondMenu,
                                            active: styles.active,
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </>
    );
};

export default MobileMenu;
