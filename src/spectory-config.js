export const HOST = `https://wp.upsidedigital.com`;
export const API_BASE = `${HOST}/wp-json/siegfried/v1`;

export const FACEBOOK_PIXEL_ID = '';
export const GOOGLE_ANALYTICS_KEY = '';
export const LINKEDIN_PIXEL_ID = '';
export const LINKEDIN_CONVERSION_ID = '';
export const HOTJAR_KEY = '';
export const GOOGLE_TAG_MANAGER_KEY = 'GTM-K87G7JG';
