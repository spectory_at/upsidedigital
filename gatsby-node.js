const { withPrefix } = require("gatsby-link")
const path = require(`path`)

module.exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions
    const pageTemplate = path.resolve(`./src/templates/page.js`)
    const jobTemplate = path.resolve(`./src/templates/jobs.js`)
    const postTemplate = path.resolve(`./src/templates/post.js`)
    const referenceTemplate = path.resolve(`./src/templates/reference.js`)
    const res = await graphql(
        `
      query {
        allWpPage {
            edges {
              node {
                id
                title
                slug
              isFrontPage
              }
            }
          }
          allWpReferenz {
            edges {
              node {
                id
                title
                uri
                locale {
                    locale
                }
                slug
              }
            }
          }
          allWpPost {
            edges {
                node {
                    id
                    title
                    dateDe: date(formatString: "DD. MMMM YYYY", locale: "de_De")
                    dateEn: date(formatString: "DD. MMMM YYYY", locale: "en_Us")
                    content
                    featuredImage {
                        node {
                          mediaItemUrl
                        }
                    }
                    excerpt
                    smTitle
                    smDescription
                    googleTitle
                    googleDescription
                    locale {
                        locale
                    }
                    slug
                }
            }
          }
            allWpJob{
            edges {
              node {
                id
                locale {
                  locale
                }
                localizedWpmlUrl
                slug
                title
              }
            }
          }
      }
    `
    )
    if (res.errors) {
        throw res.errors
    }

    const wpLanguageCodes = [
        {
            path: "",
            code: "de_DE",
        },
        {
            path: "/en/",
            code: "en_US",
        }
    ];



    res.data.allWpPage.edges.forEach(edge => {

        if (edge.node.slug) {

            let slug = edge.node.slug;
            let path = edge.node.slug;

            if (edge.node.isFrontPage) {
                path = '/';
                slug = '/';
            }

            wpLanguageCodes.forEach(language => {

                path = language.path + path;

                if (language.path !== "" && edge.node.isFrontPage) {
                    path = language.path
                }

                createPage({
                    path: `${path}`,
                    component: pageTemplate,
                    context: {
                        isFrontPage: edge.node.isFrontPage,
                        slug: edge.node.slug,
                        lang: language.code,
                    },
                })
            });



        }

    })


    res.data.allWpReferenz.edges.forEach(edge => {

        if (edge.node.slug) {

            let path = edge.node.uri;

            createPage({
                path: `${path}`,
                component: referenceTemplate,
                context: {
                    id: edge.node.id,
                    slug: edge.node.slug,
                    lang: edge.node.locale.locale,
                },
            })
        }
    });

    res.data.allWpPost.edges.forEach(edge => {
        const post = edge.node;
        const pathPrefix = post.locale.locale.startsWith('en') ? '/en/blog/' : '/blog/';
        const path = pathPrefix + post.slug;

        if (post.slug) {

            createPage({
                path: path,
                component: postTemplate,
                context: {
                    isFrontPage: false,
                    id: post.id,
                    path: path,
                    data: edge,
                    lang: post.locale.locale,
                },
            });

        }

    })

    res.data.allWpJob.edges.forEach(edge => {
        const job = edge.node;

        if (job.slug) {
            createPage({
                path: `${job.localizedWpmlUrl}`,
                component: jobTemplate,
                context: {
                    isFrontPage: false,
                    id: job.id,
                    path: job.localizedWpmlUrl,
                    lang: job.locale.locale,
                },
            })

        }
    });
}

// Implement the Gatsby API “onCreatePage”. This is
// called after every page is created.
exports.onCreatePage = async ({ page, actions }) => {
    const { createPage } = actions
    // Only update the `/app` page.
}