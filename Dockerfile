FROM node:16.6.2

ARG UID=1001
ARG GID=1001
RUN usermod -u $UID node && groupmod -g $GID node
USER node
WORKDIR /app
ENV PORT 3000
EXPOSE 3000

CMD /bin/bash